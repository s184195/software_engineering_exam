package projectTests;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import ProjectApp.Employee;
import ProjectApp.InvalidOperationException;
import ProjectApp.Project;
import ProjectApp.ProjectManager;

public class ProjectSteps {
    private StepMaster stepMaster;
    private ProjectManager projectManager;
    private Project project;
    private ArrayList<Project> preProjectList;
    private ArrayList<Project> postProjectList;

    public ProjectSteps(StepMaster stepMaster, ProjectManager projectManager){
        this.stepMaster = stepMaster;
        this.projectManager = projectManager;
    }
    
    @Given("^that a project \"([^\"]*)\" exists$")
    public void thatAProjectExists(String projectName) throws Exception {
        project = projectManager.createProject(projectName);
        stepMaster.setProject(project);
    }

  //Björn s184214
    @When("^the employee creates a new nameless project$")
    public void theEmployeeCreatesANewNamelessProject() throws Exception {
    	preProjectList = new ArrayList<>(projectManager.getProjects());
        project = projectManager.createProject();
        postProjectList = new ArrayList<>(projectManager.getProjects());
    }

  //Björn s184214
    @When("^the employee creates a project \"([^\"]*)\"$")
    public void theEmployeeCreatesAProject(String projectName) throws Exception {
    	preProjectList = new ArrayList<>(projectManager.getProjects());
        try{
            project = projectManager.createProject(projectName);
            postProjectList = new ArrayList<>(projectManager.getProjects());
        } catch (InvalidOperationException e){
            stepMaster.setErrorMessage(e.getMessage());
        }
    }


  //Björn s184214
    @Then("^the new project is added to the list of projects$")
    public void theNewProjectIsAddedToTheListOfProjects() throws Exception {
    	//Tests that only the new project was added to the list of projects
    	assertEquals(postProjectList.size(),preProjectList.size()+1);
    	for(Project p : preProjectList) {
            postProjectList.remove(p);
    	}
    	preProjectList = null;
        assertTrue(postProjectList.contains(project));
    }

  //Björn s184214
    @Then("^a new project is created with project name \"([^\"]*)\"$")
    public void aNewProjectIsCreatedWithProjectName(String projectName) throws Exception {
    	//Tests that only the new project was added to the list of projects
    	assertEquals(postProjectList.size(),preProjectList.size()+1);
    	for(Project p : preProjectList) {
            postProjectList.remove(p);
    	}
    	preProjectList = null;
        assertTrue(postProjectList.contains(projectManager.getProject(projectName)));
    }
}
