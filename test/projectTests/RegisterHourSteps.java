package projectTests;

import ProjectApp.Activities.Timelog;
import ProjectApp.ProjectManager;
import ProjectApp.Activities.ProjectActivity;
import ProjectApp.Timings.SimpleDate;
import cucumber.api.java.en.*;
import junit.framework.TestCase;

import java.util.ArrayList;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

//Wictor (s184197)
public class RegisterHourSteps {
    private StepMaster stepMaster;
    private ProjectManager projectManager;
    private ArrayList<Timelog> preTimelogs;
    private ArrayList<Timelog> postTimelogs;

    public RegisterHourSteps(StepMaster stepMaster, ProjectManager projectManager) {
        this.stepMaster = stepMaster;
        this.projectManager = projectManager;
    }

    //Wictor (s184197)
    @Then("^\"([^\"]*)\" in \"([^\"]*)\" has (\\d+.\\d+) hours logged$")
    public void inHasHoursLogged(String arg1, String arg2, double arg3) throws Exception {
        assertEquals(projectManager.getProject(arg2).getActivity(arg1).getHours(), arg3,0.1);
    }

    //Wictor (s184197)
    @Given("^that \"([^\"]*)\" in \"([^\"]*)\" has (\\d+.\\d+) hours logged$")
    public void thatInHasHoursLogged(String activityName, String projectName, double hours ) throws Exception {
        ProjectActivity projectActivity = projectManager.getProject(projectName).getActivity(activityName);
        projectActivity.getTimelog().clear();
        projectActivity.getTimelog().add(new Timelog(null, projectActivity.getStartDate(),hours));
    }

    //Wictor (s184197)
    @Then("^\"([^\"]*)\" has (\\d+.\\d+) hours logged on \"([^\"]*)\" in \"([^\"]*)\"$")
    public void hasHoursLoggedOnIn(String arg1, double arg2, String arg3, String arg4) throws Exception {
        double h = projectManager.getProject(arg4).getActivity(arg3).getHours(projectManager.getEmployee(arg1));
        assertEquals(arg2, h, 0.0);
    }

    //Wictor (s184197)
    @When("^\"([^\"]*)\" registers (-?\\d+.-?\\d+) hours spent on \"([^\"]*)\" in \"([^\"]*)\" on week (\\d+), (\\d+)$")
    public void registersHoursSpentOnInOnWeek(String employeeName, double hours, String activityName, String projectName, int week, int year){
    	projectManager.setEmployeeUsingSystem(projectManager.getEmployee(employeeName));
    	preTimelogs = new ArrayList<>(projectManager.getProject(projectName).getActivity(activityName).getTimelog());
        ProjectActivity activity = projectManager.getProject(projectName).getActivity(activityName);
        try {
            projectManager.addActivityHours(activity, new SimpleDate(year, week), hours);
        } catch (Exception e){
            stepMaster.setErrorMessage(e.getMessage());
        }
    }

    //Wictor (s184197)
    @Then("^\"([^\"]*)\" in \"([^\"]*)\" has (\\d+.\\d+) hours logged for \"([^\"]*)\" in week (\\d+), (\\d+)$")
    public void inHasHoursLoggedForWeek(String arg1, String arg2, int arg3, String arg4, int arg5, int arg6) throws Exception {
    	postTimelogs = new ArrayList<>(projectManager.getProject(arg2).getActivity(arg1).getTimelog());
    	assertEquals(postTimelogs.size(),preTimelogs.size()+1);
        int hours = 0;
        for (Timelog t : postTimelogs){
            if (t.getDate().getWeek() == arg5 && t.getDate().getYear() == arg6)
                hours += t.getHours();
        }
        TestCase.assertEquals(arg3, hours);
    }

    //Wictor (s184197)
    @Then("^\"([^\"]*)\" in \"([^\"]*)\" has (\\d+.\\d+) hours logged in week (\\d+), (\\d+)$")
    public void inHasHoursLoggedInWeek(String arg1, String arg2, int arg3, int arg4, int arg5) throws Exception {
        ArrayList<Timelog> log = projectManager.getProject(arg2).getActivity(arg1).getTimelog();
        int hours = 0;
        for (Timelog t : log){
            if (t.getDate().getWeek() == arg4 && t.getDate().getYear() == arg5)
                hours += t.getHours();
        }
        TestCase.assertEquals(arg3, hours);
    }
}