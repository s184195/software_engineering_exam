package projectTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import ProjectApp.Employee;
import ProjectApp.InvalidOperationException;
import ProjectApp.ProjectManager;
import ProjectApp.Activities.Activity;
import ProjectApp.Activities.PersonalActivity;
import ProjectApp.Activities.ProjectActivity;
import ProjectApp.Timings.SimpleDate;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class PersonalActivitySteps {
	private StepMaster stepMaster;
	private ProjectManager projectManager;
	private PersonalActivity activity;
	private Employee employee;
	private ArrayList<PersonalActivity> prePersonalActivities;
	private ArrayList<PersonalActivity> postPersonalActivities;
	
	public PersonalActivitySteps(StepMaster stepmaster, ProjectManager projectManager) {
		this.stepMaster = stepmaster;
		this.projectManager = projectManager;
	}

	//Björn s184214
	@When("the employee creates a personal activity with the reason \"([^\"]*)\" from week (\\d+) and year (\\d+) to week (\\d+) and year (\\d+)")
	public void theEmployeeCreatesAPersonalActivityWithTheReasonFromWeekAndYearToWeekAndYear(String reason, int startWeek, int startYear, int endWeek, int endYear) {
		employee = projectManager.getEmployeeUsingSystem();
		stepMaster.setEmployee(employee);
		prePersonalActivities = new ArrayList<>(projectManager.getEmployeeUsingSystem().getPersonalActivities());
		try {
			activity = projectManager.makePersonalActivityForEmployee(employee,reason, new SimpleDate(startYear, startWeek), new SimpleDate(endYear,endWeek));
		} catch (InvalidOperationException e) {
			stepMaster.setErrorMessage(e.getMessage());
		}
	}
	//Björn s184214
	@Then("the personal activity appears in the employees list of activities")
	public void thePersonalActivityAppearsInTheEmployeesListOfActivities() {
		postPersonalActivities = new ArrayList<>(projectManager.getEmployeeUsingSystem().getPersonalActivities());
		assertEquals(postPersonalActivities.size(),prePersonalActivities.size()+1);
		for(PersonalActivity p : prePersonalActivities) {
            postPersonalActivities.remove(p);
    	}
		prePersonalActivities = null;
		assertTrue(postPersonalActivities.contains(activity));
	}
}
