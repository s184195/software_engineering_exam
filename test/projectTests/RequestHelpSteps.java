package projectTests;

import ProjectApp.Activities.ProjectActivity;
import ProjectApp.Employee;
import ProjectApp.ProjectManager;
import ProjectApp.Timings.SimpleDate;
import cucumber.api.java.en.*;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class RequestHelpSteps {
    private StepMaster stepMaster;
    private ProjectManager projectManager;

    public RequestHelpSteps(StepMaster stepMaster, ProjectManager projectManager) {
        this.stepMaster = stepMaster;
        this.projectManager = projectManager;
    }

    //Wictor (s184197)
    @Given("^that activity \"([^\"]*)\" exists in \"([^\"]*)\"$")
    public void thatActivityExistsIn(String arg1, String arg2) throws Exception {
        projectManager.getProject(arg2).addActivity(new ProjectActivity(arg1, 10, new SimpleDate(2019,1), new SimpleDate(2019,5)));
    }

    //Wictor (s184197)
    @Given("^that activity \"([^\"]*)\" exists in \"([^\"]*)\" from week (\\d+), (\\d+) to week (\\d+), (\\d+)$")
    public void thatActivityExistsInFromWeekToWeek(String activityName, String projectName, int startWeek, int startYear, int endWeek, int endYear) throws Exception {
        projectManager.getProject(projectName).addActivity(new ProjectActivity(activityName, 10, new SimpleDate(startYear,startWeek), new SimpleDate(endYear,endWeek)));
    }

    @Given("^that \"([^\"]*)\" is not a participant of \"([^\"]*)\" in \"([^\"]*)\"$")
    public void thatIsNotAParticipantOfIn(String arg1, String arg2, String arg3) throws Exception {
        assertFalse(projectManager.getProject(arg3).getActivity(arg2).getParticipants().contains(projectManager.getEmployee(arg1)));
    }

    //Wictor (s184197)
    @Given("^that \"([^\"]*)\" is a participant of \"([^\"]*)\" in \"([^\"]*)\"$")
    public void thatIsAParticipantOfIn(String employeeInitials, String activityName, String projectName) throws Exception {
        projectManager.getProject(projectName).getActivity(activityName).addParticipant(projectManager.getEmployee(employeeInitials));
    }

    //Wictor (s184197)
    @When("^\"([^\"]*)\" asks \"([^\"]*)\" for help with \"([^\"]*)\" in project \"([^\"]*)\"$")
    public void asksForHelpWithInProject(String askerID, String helperID, String activityName, String projectName) throws Exception {
        projectManager.setEmployeeUsingSystem(projectManager.getEmployee(askerID));
        Employee helper = projectManager.getEmployee(helperID);
        projectManager.requestHelp(helper,projectManager.getProject(projectName).getActivity(activityName));
    }

    //Wictor (s184197)
    @Then("^\"([^\"]*)\" is a participant of \"([^\"]*)\" in \"([^\"]*)\"$")
    public void isAParticipantOfIn(String arg1, String arg2, String arg3) throws Exception {
        assertTrue(projectManager.getProject(arg3).getActivity(arg2).getParticipants().contains(projectManager.getEmployee(arg1)));
    }

    //Wictor (s184197)
    @Then("^\"([^\"]*)\" is not a participant of \"([^\"]*)\"$")
    public void isNotAParticipantOf(String arg1, String arg2) throws Exception {
        assertFalse(projectManager.getProject(arg2).getParticipants().contains(projectManager.getEmployee(arg1)));
    }

}
