package projectTests;

import cucumber.api.java.en.*;
import ProjectApp.Employee;
import ProjectApp.InvalidOperationException;
import ProjectApp.Project;
import ProjectApp.ProjectManager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

public class EmployeeSteps {
    private StepMaster stepMaster;
    private ProjectManager projectManager;
    private ArrayList<Employee> preEmployeesOnProject;
    private ArrayList<Employee> postEmployeesOnProject;
    private ArrayList<Project> preEmployeeHasProjects;
    private ArrayList<Project> postEmployeeHasProjects;

    public EmployeeSteps(StepMaster stepMaster, ProjectManager projectManager) {
        this.stepMaster = stepMaster;
        this.projectManager = projectManager;
    }

    //Wictor (s184197)
    @Given("^that the employee \"([^\"]*)\" exists$")
    public void thatAnEmployeeExists(String ID) throws Throwable {
    	Employee addedEmployee = projectManager.addEmployee(ID);
    	projectManager.setEmployeeUsingSystem(addedEmployee);
        stepMaster.setEmployee(addedEmployee);
        assertEquals(addedEmployee, projectManager.getEmployeeUsingSystem());
    }
    
  //Björn s184214
    @Then("^\"([^\"]*)\" is participating on \"([^\"]*)\"$")
    public void participatingOn(String employeeID, String projectName) throws Exception {
    	assertEquals(postEmployeeHasProjects.size(),preEmployeeHasProjects.size()+1);
    	for(Project p : preEmployeeHasProjects) {
            postEmployeeHasProjects.remove(p);
    	}
    	preEmployeeHasProjects = null;
    	assertTrue(postEmployeeHasProjects.contains(projectManager.getProject(projectName)));
    }

  //Björn s184214
    @Then("^\"([^\"]*)\" has \"([^\"]*)\" as participant$")
    public void hasAsParticipant(String projectName, String employeeID) throws Exception {
    	assertEquals(postEmployeesOnProject.size(),preEmployeesOnProject.size()+1);
    	for(Employee e : preEmployeesOnProject) {
            postEmployeesOnProject.remove(e);
    	}
    	preEmployeesOnProject = null;
    	assertTrue(postEmployeesOnProject.contains(projectManager.getEmployee(employeeID)));
    }

    //Wictor (s184197)
    @Given("^that \"([^\"]*)\" is a participant of \"([^\"]*)\"$")
    public void thatIsAParticipantOf(String arg1, String arg2) throws Exception {
        projectManager.getProject(arg2).addEmployee(projectManager.getEmployee(arg1));
    }

  //Björn s184214
    @When("^\"([^\"]*)\" adds \"([^\"]*)\" to project \"([^\"]*)\"$")
    public void addsToProject(String projectLeader, String employeeID, String projectName) throws Exception {
        Project p = projectManager.getProject(projectName);
        preEmployeesOnProject = new ArrayList<>(projectManager.getProject(projectName).getParticipants());
    	preEmployeeHasProjects = new ArrayList<>(projectManager.getEmployee(employeeID).getProjects());
    	try {
    		if (p.getProjectLeader().getInitials().equals(projectLeader)){
    			projectManager.getProject(projectName).addEmployee(projectManager.getEmployee(employeeID));
    			projectManager.getEmployee(employeeID).addProject(projectManager.getProject(projectName));
    			postEmployeeHasProjects = new ArrayList<>(projectManager.getEmployee(employeeID).getProjects());
    			postEmployeesOnProject = new ArrayList<>(projectManager.getProject(projectName).getParticipants());
    		}
    		else{
    			stepMaster.setErrorMessage("You do not have permission to do this.");
    		}
    	} catch (InvalidOperationException e){
            stepMaster.setErrorMessage(e.getMessage());
        }
    }
}