package projectTests;

import ProjectApp.Timings.SimpleDate;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.Locale;

import static org.junit.Assert.assertEquals;

public class SimpleDateSteps {
	private SimpleDate x;
	private SimpleDate y;
	private LocalDate xAsLocal;
	public SimpleDateSteps() {}

	//Björn s184214
	@Given("^a Simple Date X with year (\\d+) and weekNum (\\d+)$")
	public void aSimpleDateXWithYearAndWeekNum(int year, int week) throws Exception {
		x = new SimpleDate(year, week);

	}

	//Björn s184214
	@Given("^a Simple Date Y with year (\\d+) and weekNum (\\d+)$")
	public void aSimpleDateYWithYearAndWeekNum(int year, int week) throws Exception {
		y = new SimpleDate(year, week);
	}

	//Björn s184214
	@Given("^a LocalDate X with year (\\d+) and week (\\d+)$")
	public void aLocalDateXWithYearAndWeekNum(int year, int week) throws Exception {
		// -1 since week of year starts at 1.
		xAsLocal = LocalDate.MIN.withYear(year).plusWeeks(week - 1);
	}

	//Björn s184214
	@Then("^the compareTo function returns (-?\\d+)$")
	public void theCompareToFunctionReturns(int diffWeek) throws Exception {
		int diff = x.compareTo(y);
		assertEquals(new Integer(diffWeek), new Integer(diff));
	}

	//Olav (s184195)
	@When("^converting X to a LocalDate$")
	public void convertingXToALocalDate() throws Exception {
		xAsLocal = x.toLocalDate();
	}

	//Olav (s184195)
	@Then("^X has the LocalDate fields (\\d+) and (\\d+)$")
	public void xHasTheLocalDateFieldsAnd(int year, int week) throws Exception {
		assertEquals(xAsLocal.getYear(), year);

		WeekFields weekFields = WeekFields.of(Locale.getDefault());
		assertEquals(xAsLocal.get(weekFields.weekOfYear()),week);
	}

	//Olav (s184195)
	@When("^converting X to a SimpleDate$")
	public void convertingXToASimpleDate() throws Exception {
		x = new SimpleDate(xAsLocal);
	}

	//Olav (s184195)
	@Then("^X has the SimpleDate fields (\\d+) and (\\d+)$")
	public void xHasTheSimpleDateFieldsAnd(int year, int week) throws Exception {
		assertEquals(week, x.getWeek());
		assertEquals(year, x.getYear());
	}
}
