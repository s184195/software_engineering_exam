package projectTests;

import cucumber.api.java.en.*;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import ProjectApp.Employee;
import ProjectApp.InvalidOperationException;
import ProjectApp.Project;
import ProjectApp.ProjectManager;

import static org.junit.Assert.assertEquals;

//Laura (s184234)
public class ProjectLeaderSteps {
    private StepMaster stepMaster;
    private ProjectManager projectManager;

    public ProjectLeaderSteps(StepMaster stepMaster, ProjectManager projectManager){
        this.stepMaster = stepMaster;
        this.projectManager = projectManager;
    }

    @Given("^that \"([^\"]*)\" is the project leader of \"([^\"]*)\"$")
    public void thatIsTheProjectLeaderOf(String employeeName, String projectName) throws Exception {
            Project p = projectManager.getProject(projectName);
            Employee e = projectManager.getEmployee(employeeName);
            projectManager.appointProjectLeader(p,e);
    }

    @When("^an employee appoints \"([^\"]*)\" as project leader$")
    public void anEmployeeAppointsAsProjectLeader(String employeeName){
        try {
            projectManager.appointProjectLeader(stepMaster.getProject(),projectManager.getEmployee(employeeName));
        } catch (InvalidOperationException e){
            stepMaster.setErrorMessage(e.getMessage());
        }
    }

    @Then("^the projectís leader is \"([^\"]*)\"$")
    public void theProjectSLeaderIs(String employeeName) throws Exception {
    	assertEquals(employeeName,stepMaster.getProject().getProjectLeader().getInitials());
    }
}