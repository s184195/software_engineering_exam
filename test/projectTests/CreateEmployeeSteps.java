package projectTests;

import cucumber.api.java.en.*;
import ProjectApp.Employee;
import ProjectApp.InvalidOperationException;
import ProjectApp.ProjectManager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

public class CreateEmployeeSteps {
	private StepMaster stepMaster;
    private ProjectManager projectManager;
    private ErrorSteps errorSteps;
    private ArrayList<Employee> preEmployeeList;
    private ArrayList<Employee> postEmployeeList;
    private Employee employee;
    
    public CreateEmployeeSteps(StepMaster stepMaster, ProjectManager projectManager) {
        this.stepMaster = stepMaster;
        this.projectManager = projectManager;
    }
    
  //Björn s184214
    @Then("^an employee with name \"([^\"]*)\" is successfully created$")
    public void anEmployeeWithNameIsSuccessfullyCreated(String employeeID) throws Exception {
    	assertEquals(postEmployeeList.size(),preEmployeeList.size()+1);
    	for(Employee e : preEmployeeList) {
            postEmployeeList.remove(e);
    	}
    	assertTrue(postEmployeeList.contains(employee));
    }
  //Björn s184214
    @Then("^the user gets the error message \"([^\"]*)\"$")
    public void theUserGetsTheErrorMessage(String errorMessage) throws Exception {
    	errorSteps = new ErrorSteps(stepMaster, projectManager);
    	errorSteps.theEmployeeGetsTheErrorMessage(errorMessage);
    }
    
  //Björn s184214
    @When("^the user creates an employee with name \"([^\"]*)\"$")
    public void theUserCreatesAnEmployeeWithName(String employeeID) throws Exception {
        preEmployeeList = new ArrayList<>(projectManager.getEmployees());
        try {
            employee = projectManager.addEmployee(employeeID);
        } catch (InvalidOperationException e) {
            stepMaster.setErrorMessage(e.getMessage());
        }
        postEmployeeList = new ArrayList<>(projectManager.getEmployees());
    }
}