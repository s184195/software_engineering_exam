package projectTests;

import ProjectApp.ProjectManager;
import cucumber.api.java.en.*;
import ProjectApp.Employee;
import ProjectApp.InvalidOperationException;
import ProjectApp.ProjectManager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
//Björn s184214
public class CalculateProjectProgress {
	private StepMaster stepMaster;
    private ProjectManager projectManager;
    
    public CalculateProjectProgress(StepMaster stepMaster, ProjectManager projectManager) {
    	this.stepMaster = stepMaster;
    	this.projectManager = projectManager;
    }
    
    @When("^the system calculates the project progress for the project \"([^\"]*)\" the value (\\d+) is returned$")
    public void theSystemCalculatesTheProjectProgressForTheProjectTheValueIsReturned(String projectName, int hours) throws Exception {
    	double[] weeklyProgress = projectManager.getProject(projectName).computeProjectProgress();
    	double sumProgress = 0.0;
    	for(double d : weeklyProgress) {
    		sumProgress += d;
    	}
    	assertEquals((double) hours, sumProgress,0.0);
    }
}
