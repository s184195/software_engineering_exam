package projectTests;

import cucumber.api.java.en.Then;
import ProjectApp.ProjectManager;

import static org.junit.Assert.assertEquals;

//Olav (s184195)
public class ErrorSteps {
    private StepMaster stepMaster;
    private ProjectManager projectManager;

    public ErrorSteps(StepMaster stepMaster, ProjectManager projectManager){
        this.stepMaster = stepMaster;
        this.projectManager = projectManager;
    }

    @Then("^the employee gets the error message \"([^\"]*)\"$")
    public void theEmployeeGetsTheErrorMessage(String errorMessage) throws Exception {
        assertEquals(errorMessage,stepMaster.getErrorMessage());
    }
}