package projectTests;

import ProjectApp.Activities.ProjectActivity;
import ProjectApp.Timings.SimpleDate;
import cucumber.api.PendingException;
import cucumber.api.java.en.*;
import ProjectApp.*;

import static org.junit.Assert.*;

import java.util.ArrayList;

public class ProjectActivitySteps {
    private StepMaster stepMaster;
    private ProjectManager projectManager;

    private Project project;
    private Employee projectLeader;
    private ProjectActivity projectActivity;
    private ArrayList<Employee> preActivityList;
    private ArrayList<Employee> postActivityList;
    private ArrayList<ProjectActivity> preEmployeeActivities;
    private ArrayList<ProjectActivity> postEmployeeActivities;
    private ErrorSteps errorSteps;

    public ProjectActivitySteps(StepMaster stepMaster, ProjectManager projectManager) {
        this.stepMaster = stepMaster;
        this.projectManager = projectManager;
    }

    //Laura (s184234)
    @Given("^a project \"([^\"]*)\" with a project leader \"([^\"]*)\"$")
    public void aProjectWithAProjectLeader(String projectName, String pLeader) throws Exception {
        project = projectManager.createProject(projectName);
        projectLeader = projectManager.addEmployee(pLeader);
        projectManager.appointProjectLeader(project, projectLeader);

        stepMaster.setProject(project);
    }

    //Laura (s184234)
    @When("^the employee \"([^\"]*)\" creates a project activity \"([^\"]*)\" with estimated time (\\d+) hours and starting on week (\\d+) in (\\d+) ending on week (\\d+) in (\\d+)$")
    public void theEmployeeCreatesAProjectActivityWithEstimatedTimeHoursAndStartingOnWeekInEndingOnWeekIn(String employeeID, String activityName, int hours, int startWeek, int startYear, int endWeek, int endYear) throws Exception {
        projectManager.setEmployeeUsingSystem(projectManager.getEmployee(employeeID));
        try {
            projectActivity = projectManager.createActivityInProject(project, activityName, hours,  new SimpleDate(startYear,startWeek), new SimpleDate(endYear,endWeek));
            stepMaster.setActivity(projectActivity);
        } catch (InvalidOperationException e) {
            stepMaster.setErrorMessage(e.getMessage());
        }
    }

    //Laura (s184234)
    @Then("the project has a project activity called \"([^\"]*)\" with (\\d+) allocated hours spanning from week (\\d+) to (\\d+)$")
    public void hasAProjectActivityCalledWithAllocatedHoursSpanningFromWeekTo(String name, int hours, int startWeek, int endWeek) throws Exception {
        assertEquals(stepMaster.getActivity(), projectActivity);
    }

    //Laura (s184234)
    @Given("^the project \"([^\"]*)\" has a project activity \"([^\"]*)\"$")
    public void theProjectHasAProjectActivity(String project, String activityName) throws Throwable {
        projectActivity = new ProjectActivity(activityName,2, new SimpleDate(2000,2), new SimpleDate(2000,4));
        stepMaster.getProject().addActivity(projectActivity);
        stepMaster.setActivity(projectActivity);
    }

  //Björn s184214
    @When("^the project leader of \"([^\"]*)\" adds \"([^\"]*)\" to \"([^\"]*)\"$")
    public void theProjectLeaderOfAddsTo(String project, String employeeID, String activityName) throws Throwable {
    	preActivityList = new ArrayList<Employee>(stepMaster.getActivity().getParticipants());
    	preEmployeeActivities = new ArrayList<ProjectActivity>(projectManager.getEmployee(employeeID).getProjectActivities());
    	try {
    	projectManager.addEmployeeToProjectActivity(stepMaster.getProject(),stepMaster.getActivity(),projectManager.getEmployee(employeeID));
    	} catch (InvalidOperationException e) {
    		stepMaster.setErrorMessage(e.getMessage());
    	}
    }

    // Working on activity
  //Björn s184214
    @And("the list of activities for the employee \"([^\"]*)\" contains the activity \"([^\"]*)\"$")
    public void theListOfActivitiesForTheEmployeeContainsTheActivity(String ID, String activity) throws Throwable {
    	postEmployeeActivities = new ArrayList<ProjectActivity>(projectManager.getEmployee(ID).getProjectActivities());
    	assertEquals(postEmployeeActivities.size(),preEmployeeActivities.size()+1);
    	for(ProjectActivity p : preEmployeeActivities) {
            postEmployeeActivities.remove(p);
    	}
    	preEmployeeActivities = null;
        assertTrue(postEmployeeActivities.contains(projectActivity));
    }

  //Björn s184214
    @Then("^\"([^\"]*)\" is part of the list of employees working on the activity \"([^\"]*)\"$")
    public void isPartOfTheListOfEmployeesWorkingOn(String employeeName, String activityName) throws Throwable {
    	//Does not check whether previous members still remain within activity
    	postActivityList = new ArrayList<Employee>(stepMaster.getActivity().getParticipants());
    	assertEquals(postActivityList.size(),preActivityList.size()+1);
    	for(Employee e : preActivityList) {
            postActivityList.remove(e);
    	}
    	preActivityList = null;
        assertTrue(postActivityList.contains(projectManager.getEmployee(employeeName)));
    }

    //Laura (s184234)
    @Given("^that the employee \"([^\"]*)\" is working on the activity \"([^\"]*)\"$")
    public void thatTheEmployeeIsWorkingOnTheActivity(String employeeID, String activity) throws Exception {
    	projectManager.addEmployeeToProjectActivity(stepMaster.getProject(),stepMaster.getActivity(),projectManager.getEmployee(employeeID));
    }

    //Laura (s184234)
    @Given("^that the employee \"([^\"]*)\" is participating on \"([^\"]*)\"$")
    public void thatTheEmployeeIsParticipatingOn(String employeeID, String projectName) throws Exception {
    	project = projectManager.getProject(projectName);
    	project.addEmployee(projectManager.getEmployee(employeeID));
    }

    //Laura (s184234)
    @Given("^that the employee \"([^\"]*)\" has (\\d+) project activities at the same time$")
    public void thatTheEmployeeHasProjectActivitiesAtTheSameTime(String employeeID, int num) throws Exception {
    	for(int i = 0; i < num; i++) {
    		project.createProjectActivity("A"+i, 2, new SimpleDate(2000,2) , new SimpleDate(2000,4));
    		project.getActivity("A"+i).addParticipant(projectManager.getEmployee(employeeID));
    	}
    }

    //Laura (s184234)
    @Given("^that the employee \"([^\"]*)\" has a personal activtity in the given period$")
    public void thatTheEmployeeHasAPersonalActivtityInTheGivenPeriod(String employeeID) throws Exception {
    	projectManager.getEmployee(employeeID).makePersonalActivity("Hospital",2,2000,4,2000);
    }

    //Laura (s184234)
    @Then("^the project leader gets the error message \"([^\"]*)\"$")
    public void theProjectLeaderGetsTheErrorMessage(String errorMessage) throws Exception {
    	errorSteps = new ErrorSteps(stepMaster,projectManager);
    	errorSteps.theEmployeeGetsTheErrorMessage(errorMessage);
    }

    //Olav (s184195)
    @Given("^that \"([^\"]*)\" is not the project leader of \"([^\"]*)\"$")
    public void thatIsNotTheProjectLeaderOf(String employeeID, String projectName) throws Exception {
        assertNotEquals(projectManager.getProject(projectName).getProjectLeader().getInitials(), employeeID);
    }

    //Olav (s184195)
    @Given("^the project \"([^\"]*)\" does not have an activity \"([^\"]*)\"$")
    public void theProjectDoesNotHaveAnActivity(String projectName, String activityName) throws Exception {
        assertNull(projectManager.getProject(projectName).getActivity(activityName));
    }

    //Olav (s184195)
    @When("^the project leader changes the name of \"([^\"]*)\" in \"([^\"]*)\" to \"([^\"]*)\"$")
    public void theProjectLeaderChangesTheNameOfInTo(String oldActivityName, String projectName, String newActivityName){
        Project project = projectManager.getProject(projectName);
        try {
            projectManager.updateProjectActivityName(project,project.getActivity(oldActivityName),newActivityName);
        } catch (InvalidOperationException e) {
            stepMaster.setErrorMessage(e.getMessage());
        }
    }

    //Olav (s184195)
    @Then("^there is no activity called \"([^\"]*)\" in \"([^\"]*)\"$")
    public void thereIsNoActivityCalledIn(String activityName, String projectName) throws Exception {
        assertNull(projectManager.getProject(projectName).getActivity(activityName));
    }

    //Olav (s184195)
    @When("^the project leader changes the starting date of \"([^\"]*)\" in \"([^\"]*)\" to week (\\d+), (\\d+)$")
    public void theProjectLeaderChangesTheStartingDateOfInToWeek(String activityName, String projectName, int week, int year){
        try {
            projectManager.updateStartDateInActivity(projectManager.getProject(projectName).getActivity(activityName), new SimpleDate(year,week));
        } catch (InvalidOperationException e) {
            stepMaster.setErrorMessage(e.getMessage());
        }
    }

    //Olav (s184195)
    @When("^the project leader changes the end date of \"([^\"]*)\" in \"([^\"]*)\" to week (\\d+), (\\d+)$")
    public void theProjectLeaderChangesTheEndDateOfInToWeek(String activityName, String projectName, int week, int year) {
        try {
            projectManager.updateEndDateInActivity(projectManager.getProject(projectName).getActivity(activityName), new SimpleDate(year,week));
        } catch (InvalidOperationException e) {
            stepMaster.setErrorMessage(e.getMessage());
        }
    }

    //Olav (s184195)
    @Then("^the project \"([^\"]*)\" has a project activity called \"([^\"]*)\" spanning from week (\\d+), (\\d+) to (\\d+), (\\d+)$")
    public void theProjectHasAProjectActivityCalledSpanningFromWeekTo(String projectName, String activityName, int startWeek, int startYear, int endWeek, int endYear) throws Exception {
        assertEquals(projectManager.getProject(projectName).getActivity(activityName).getStartDate().compareTo(new SimpleDate(startYear,startWeek)), 0);
        assertEquals(projectManager.getProject(projectName).getActivity(activityName).getEndDate().compareTo(new SimpleDate(endYear,endWeek)),0);
    }


}
