package ProjectApp;

//Olav (s184195)
public class InvalidOperationException extends Exception {
    public InvalidOperationException(String errorMessage){
        super(errorMessage);
    }
}
