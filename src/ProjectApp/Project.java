package ProjectApp;

import ProjectApp.Activities.ProjectActivity;
import ProjectApp.Timings.SimpleDate;

import java.util.ArrayList;

public class Project {
    private int id;
    private Employee projectLeader;
    private String projectName;
    private ArrayList<ProjectActivity> activities = new ArrayList<>();
    private ArrayList<Employee> participants = new ArrayList<>();


    public Project(int id){this.id = id;}
    public Project(int id, String projectName) {
        this.id = id;
        this.projectName = projectName;
    }

    //Getters
    public ArrayList<Employee> getParticipants() {
        return participants;
    }

    public ArrayList<ProjectActivity> getActivities() {
        return activities;
    }

    public String getProjectName() {
        return projectName;
    }

    public int getId() {
        return id;
    }


    public Employee getProjectLeader() {
        return projectLeader;
    }

    //Setters
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    //For JUnit steps
    public void addActivity(ProjectActivity p){
        activities.add(p);
    }

    //Methods

    //Wictor (s184197)
    public void addEmployee(Employee e) throws InvalidOperationException {
        if (!participants.contains(e))
            participants.add(e);
        else
            throw new InvalidOperationException("Employee is already on the project.");
    }

    //Olav (s184195)
    public void addEmployeeToProjectActivity(ProjectActivity a, Employee e) throws InvalidOperationException {
        if (!activities.contains(a))
            throw new InvalidOperationException("Project activity is not in project");

        a.addParticipant(e);

        //Add employee to project if they're not already in it
        if (!participants.contains(e))
            addEmployee(e);
    }

    //Wictor (s184197)
    public ProjectActivity getActivity(String name){
        for (ProjectActivity a : activities){
            if (a.getName().equals(name))
                return a;
        }
        return null;
    }

    // Laura (s184234)
    public void forceProjectLeader(Employee employee) {
        this.projectLeader = employee;
        if (!participants.contains(employee))
            participants.add(employee);
    }

    //Olav (s184195)
    public void setProjectLeader(Employee employee) throws InvalidOperationException {
        if (projectLeader != null)
            throw new InvalidOperationException("Project already has a project leader");
        this.projectLeader = employee;
        if (!participants.contains(employee))
            participants.add(employee);
    }

    // Creates and adds new project activity
    public ProjectActivity createProjectActivity(String name, double hours, SimpleDate startDate, SimpleDate endDate) throws InvalidOperationException {
        ProjectActivity activity = new ProjectActivity(name, hours, startDate, endDate);
        activities.add(activity);
        activity.setParentProject(this);
        return activity;
    }

    //Olav (s184195)
    //Generates a histogram over logged hours in each week
    public double[] computeProjectProgress(){
        SimpleDate startDate = getMinDate();
        SimpleDate endDate = getMaxDate();

        int weekDiff = startDate.compareTo(endDate) + 1;
        double[] weeks = new double[weekDiff];

        for (ProjectActivity a: activities){
            double[] weeklyProgess = a.getWeeklyProgress();
            int index = startDate.compareTo(a.getStartDate());
            for (int i = 0; i < weeklyProgess.length; i++)
                weeks[i + index] += weeklyProgess[i];
        }
        return weeks;
    }

    //Olav (s184195)
    public SimpleDate getMinDate(){
        SimpleDate startDate = new SimpleDate(Integer.MAX_VALUE,Integer.MAX_VALUE);
        for (ProjectActivity a: activities)
            if (a.getStartDate().compareTo(startDate) > 0)
                startDate = a.getStartDate();
        return startDate;
    }

    //Olav (s184195)
    public SimpleDate getMaxDate(){
        SimpleDate endDate = new SimpleDate(Integer.MIN_VALUE + 3000,Integer.MIN_VALUE + 3000);
        for (ProjectActivity a: activities)
            if (a.getEndDate().compareTo(endDate) < 0)
                endDate = a.getEndDate();
        return endDate;
    }

    //Olav (s184195)
    public void updateProjectActivityName(ProjectActivity p, String newName) throws InvalidOperationException {
        if (newName == null || "".equals(newName.trim()))
            throw new InvalidOperationException("Project activity name may not be empty");
        for (ProjectActivity pa: activities)
            if (p != pa && newName.equals(pa.getName()))
                throw new InvalidOperationException("Project activity name is not unique");
        p.setName(newName);
    }

    //Olav (s184195)
    @Override
    public String toString() {
        if (projectName != null)
            return String.format("%s #%d",projectName,id);
        else
            return String.format("#%d",id);
    }
}
