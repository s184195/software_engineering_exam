package ProjectApp;

import ProjectApp.Activities.Activity;
import ProjectApp.Activities.PersonalActivity;
import ProjectApp.Activities.ProjectActivity;
import ProjectApp.Timings.SimpleDate;

import java.util.ArrayList;

//Olav (s184195)
public class Employee {
    public final static int MAX_PROJECTS = 20;

    private String initials; //Initials are unique to all employees
    private ArrayList<Project> projects = new ArrayList<>();
    private ArrayList<ProjectActivity> projectActivities = new ArrayList<>();
    private ArrayList<PersonalActivity> personalActivities = new ArrayList<>();

    public Employee(String id){
        this.initials = id;
    }

    //GETTERS
    public String getInitials() {
        return initials;
    }

    public ArrayList<ProjectActivity> getProjectActivities() {
        return projectActivities;
    }

    public ArrayList<PersonalActivity> getPersonalActivities(){
        return personalActivities;
    }

    public ArrayList<Activity> getActivities() {
        ArrayList<Activity> activities = new ArrayList<>();
        activities.addAll(personalActivities);
        activities.addAll(projectActivities);
        return activities;
    }

    public ArrayList<Project> getProjects() {
    	return projects;
    }

    //Methods
    public void addProject(Project project) {
    	projects.add(project);
    }
    
    public void addProjectActivity(ProjectActivity a) {
        projectActivities.add(a);
    }

    public PersonalActivity makePersonalActivity(String name, int startWeek, int startYear,int endWeek, int endYear) throws InvalidOperationException {
    	return makePersonalActivity(name, new SimpleDate(startYear, startWeek), new SimpleDate(endYear, endWeek));
    }

    public PersonalActivity makePersonalActivity(String name, SimpleDate startDate, SimpleDate endDate) throws InvalidOperationException {
        PersonalActivity activity = new PersonalActivity(name,startDate,endDate);
        personalActivities.add(activity);
        return activity;
    }

    //Sees if the employee is free (has less activities than max) within the given period.
    public boolean isFreeWithinPeriod(SimpleDate startDate, SimpleDate endDate){
        int[] projectCount = occupiedWeeks(startDate, endDate);
        for (int i = 0; i < projectCount.length; i++)
            if (Math.abs(projectCount[i]) - 1 >= MAX_PROJECTS)
                return false;
        return true;
    }

    //Wictor (s184197)
    //Returns the amount of activities the employee is assigned to
    //The array has the structure: No. of activities + 1, negative if a personal activity is present
    public int[] occupiedWeeks(SimpleDate startDate, SimpleDate endDate){
        //Compute number of weeks the SimpleDates span over
        int spanningWeeks = startDate.compareTo(endDate) + 1;
        int[] activityCount = new int[spanningWeeks];

        //Initialise array to 1
        for (int i = 0; i < spanningWeeks; i++)
            activityCount[i] = 1;

        //Count each project activity
        for (Activity a: getActivities()){
            int startIndex = Math.max(startDate.compareTo(a.getStartDate()),0);
            int endIndex =  Math.min(spanningWeeks + endDate.compareTo(a.getEndDate()),spanningWeeks);

            if (a instanceof ProjectActivity){
                //Increment the spanning weeks
                for (int i = startIndex; i < endIndex; i++)
                    if (activityCount[i] > 0 )
                        activityCount[i]++;
                    else
                        activityCount[i]--;
            } else
                //Vanguard for occupation by personal activities
                for (int i = startIndex; i < endIndex; i++)
                    if (activityCount[i] > 0)
                        activityCount[i] *= -1;

        }
        return activityCount;
    }
}
