package ProjectApp.Database;

import ProjectApp.Activities.Activity;
import ProjectApp.Activities.PersonalActivity;
import ProjectApp.Activities.ProjectActivity;
import ProjectApp.Activities.Timelog;
import ProjectApp.Employee;
import ProjectApp.InvalidOperationException;
import ProjectApp.Project;
import ProjectApp.ProjectManager;
import ProjectApp.Timings.SimpleDate;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.Iterator;

// Laura (s184234)
public class DatabaseJSON implements Database {
    private ProjectManager projectManager;

    private JSONParser jsonParser;
    private String filePath = "database.json";

    private JSONObject jsonObject;

    private JSONArray employeeList;
    private JSONArray projectList;

    public DatabaseJSON() {}

    @Override
    public void load(ProjectManager projectManager) {
        // Loads the saved data into the program at launch

        this.projectManager = projectManager;

        File JSONfile = new File(filePath);

        try {
            if (JSONfile.createNewFile()) {
                save();
            } else {
                // Reads from existing database
                jsonParser = new JSONParser();
                Reader reader = new FileReader(filePath);

                // Read JSON file
                jsonObject = (JSONObject) jsonParser.parse(reader);

                readTimeManager();
                readEmployeeList();
                readProjectList();
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    private void readTimeManager() {
        // Parses the time manager object
        JSONObject timeManagerObject = (JSONObject) jsonObject.get("time manager");

        Long workingYearLong = (Long) timeManagerObject.get("year");
        int workingYear = workingYearLong.intValue();

        Long projectsCreatedThisYearLong = (Long) timeManagerObject.get("projects created this year");
        int projectsCreatedThisYear = projectsCreatedThisYearLong.intValue();

        projectManager.getTimeManager().setWorkingYear(workingYear);
        projectManager.getTimeManager().setProjectsCreatedThisYear(projectsCreatedThisYear);
        projectManager.getTimeManager().updateYear();

    }

    private void readEmployeeList() {
        // Parses employee list from json list
        // Turn initials into new employee objects and adds them to employees list
        employeeList = (JSONArray) jsonObject.get("employees");
        Iterator<JSONObject> employeeIterator = employeeList.iterator();
        while (employeeIterator.hasNext()) {
            JSONObject nextEmployee = employeeIterator.next();

            String initials = (String) nextEmployee.get("initials");

            Employee employee = new Employee(initials);

            projectManager.getEmployees().add(employee);
            readPersonalActivityList(nextEmployee, employee);

        }
    }

    private void readProjectList() {
        // Parses project list from json list

        projectList = (JSONArray) jsonObject.get("projects");
        Iterator<JSONObject> projectIterator = projectList.iterator();
        while (projectIterator.hasNext()) {
            JSONObject nextProject = projectIterator.next();

            // Create project with at least an ID
            Long ID = (Long) nextProject.get("id");
            int projectID = ID.intValue();
            Project project = new Project(projectID);

            // Set project leader
            if (nextProject.get("leader") != null) {
                String projectLeader = (String) nextProject.get("leader"); //TODO: if no leader
                project.forceProjectLeader(projectManager.getEmployee(projectLeader));
            }

            // Set project name
            if (nextProject.get("name") != null) {
                String projectName = (String) nextProject.get("name");
                project.setProjectName(projectName);
            }

            projectManager.getProjects().add(project);
            readProjectActivityList(nextProject, project);

        }
    }

    private void readProjectActivityList(JSONObject proj, Project project) {
        // Parses the activity list of a project, including their timelogs, participant lists, etc.

        JSONArray projectActivityList = (JSONArray) proj.get("activities");

        Iterator<JSONObject> projectActivityIterator = projectActivityList.iterator();
        // Iterates through each activity in the list
        while (projectActivityIterator.hasNext()) {
            JSONObject nextProjectActivity = projectActivityIterator.next();

            String name = (String) nextProjectActivity.get("name");
            SimpleDate startDate = getDate(nextProjectActivity, "start ");
            SimpleDate endDate = getDate(nextProjectActivity, "end ");
            double hours = (Double) nextProjectActivity.get("allocated hours");

            try {
                // Add the new project activity with the saved name, date and allocated hours
                ProjectActivity projectActivity = new ProjectActivity(name, hours, startDate, endDate);
                projectActivity.setParentProject(project);
                project.getActivities().add(projectActivity);

                // Read participant list
                JSONArray participantList = (JSONArray) nextProjectActivity.get("participants");
                Iterator<JSONObject> participantIterator = participantList.iterator();
                while (participantIterator.hasNext()) {
                    JSONObject nextParticipant = participantIterator.next();
                    String initials = (String) nextParticipant.get("initials");

                    project.addEmployeeToProjectActivity(projectActivity,projectManager.getEmployee(initials));
                }

                // Read timelog
                JSONArray timeLog = (JSONArray) nextProjectActivity.get("time log");
                Iterator<JSONObject> timeLogIterator = timeLog.iterator();
                while (timeLogIterator.hasNext()) {
                    JSONObject nextTimeLogEntry = timeLogIterator.next();

                    String initials = (String) nextTimeLogEntry.get("employee");
                    double loggedHours = (double) nextTimeLogEntry.get("hours");

                    // Convert date into simpleDate
                    SimpleDate date = getDate(nextTimeLogEntry, "");

                    Timelog timelogEntry = new Timelog(projectManager.getEmployee(initials), date, loggedHours);
                    projectActivity.getTimelog().add(timelogEntry);
                }
            } catch (InvalidOperationException e) {
                e.printStackTrace();
            }
        }

    }

    private SimpleDate getDate(JSONObject obj, String option) {
        // Gets a date from a JSONObject and returns it as a simpledate
        // String "option" must be either "start", "end" or ""

        Long week = (Long) obj.get(String.format("%s", option) + "week");
        Long year = (Long) obj.get(String.format("%s", option) + "year");

        return new SimpleDate(year.intValue(), week.intValue());
    }

    private void readPersonalActivityList(JSONObject emp, Employee employee) {
        // Reads the personal activity list of an employee
        JSONArray personalActivityList = (JSONArray) emp.get("personal activities");

        Iterator<JSONObject> personalActivityIterator = personalActivityList.iterator();
        while (personalActivityIterator.hasNext()) {
            JSONObject nextPersonalActivity = personalActivityIterator.next();

            String name = (String) nextPersonalActivity.get("name");

            SimpleDate startDate = getDate(nextPersonalActivity, "start ");
            SimpleDate endDate = getDate(nextPersonalActivity, "end ");

            PersonalActivity p = null;
            try {
                // Create the new personal activity with the saved data
                p = new PersonalActivity(name, startDate, endDate);
            } catch (InvalidOperationException e) {
                e.printStackTrace();
            }
            employee.getPersonalActivities().add(p);
        }
    }

    @Override
    public void save() {
        // Upon exiting program, the data is converted to JSONObjects and saved into database.json

        writeEmployeeList();
        writeProjectList();

        JSONObject data = new JSONObject();
        data.put("employees", employeeList);
        data.put("projects", projectList);
        data.put("time manager", writeTimeManager());

        // Writes employee list to file
        try (FileWriter file = new FileWriter(filePath)) {

            file.write(data.toJSONString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    JSONObject writeTimeManager() {
        // Puts the time manager into a JSONObject

        JSONObject timeManagerObject = new JSONObject();
        int workingYear = projectManager.getTimeManager().getWorkingYear();
        int projectsCreatedThisYear = projectManager.getTimeManager().getProjectsCreatedThisYear();

        timeManagerObject.put("year", workingYear);
        timeManagerObject.put("projects created this year", projectsCreatedThisYear);

        return timeManagerObject;
    }

    void writeEmployeeList() {
        // Creates JSONObject for each employee
        employeeList = new JSONArray();

        if (!projectManager.getEmployees().isEmpty()) {
            for (Employee e : projectManager.getEmployees()) {

                JSONObject employeeObject = new JSONObject();
                employeeObject.put("initials", e.getInitials());
                employeeObject.put("personal activities", writePersonalActivityList(e));

                employeeList.add(employeeObject);
            }
        }
    }

    private JSONArray writePersonalActivityList(Employee employee) {
        // Iterates through an employee's personal activities and returns them in a JSONArray
        JSONArray personalActivityList = new JSONArray();

        for (PersonalActivity a : employee.getPersonalActivities()) {
            JSONObject personalActivityObject = new JSONObject();
            personalActivityObject.put("name", a.getName());
            formatActivityDates(personalActivityObject,a);
            personalActivityList.add(personalActivityObject);
        }
        return personalActivityList;
    }

    private void writeProjectList() {
        // Creates a JSONArray with all the projects in the ProjectManager
        projectList = new JSONArray();

        // Goes through each project
        for (Project p : projectManager.getProjects()) {
            JSONObject projectObject = new JSONObject();

            // Puts project ID, name and leader into a JSONObject
            String name;
            String leader;

            if (p.getProjectName() != null) name = p.getProjectName();
            else name = null;

            if (p.getProjectLeader() != null) leader = p.getProjectLeader().getInitials();
            else leader = null;

            projectObject.put("id", p.getId());
            projectObject.put("name", name);
            projectObject.put("leader", leader);

            // Saves activities
            projectObject.put("activities", writeProjectActivityList(p));

            // Saves in JSONArray list of projects
            projectList.add(projectObject);
        }
    }

    private JSONArray writeProjectActivityList(Project project) {
        // Puts a project's activities into a JSONArray
        JSONArray projectActivityList = new JSONArray();

        for (ProjectActivity a : project.getActivities()) {
            JSONObject projectActivityObject = new JSONObject();

            // Gets participants of the activity
            JSONArray participantList = new JSONArray();
            for (Employee e : a.getParticipants()) {
                JSONObject employeeObject = new JSONObject();
                employeeObject.put("initials", e.getInitials());
                participantList.add(employeeObject);
            }

            projectActivityObject.put("name", a.getName());
            projectActivityObject.put("allocated hours", a.getHoursAllocated());
            projectActivityObject.put("participants", participantList);

            formatActivityDates(projectActivityObject,a);

            // Saves every timelog entry
            JSONArray timeLog = new JSONArray();
            for (Timelog t : a.getTimelog()) {
                JSONObject timeLogObject = new JSONObject();
                timeLogObject.put("employee", t.getEmployee().getInitials());
                timeLogObject.put("week", t.getDate().getWeek());
                timeLogObject.put("year", t.getDate().getYear());
                timeLogObject.put("hours", t.getHours());
                timeLog.add(timeLogObject);
            }

            projectActivityObject.put("time log", timeLog);
            projectActivityList.add(projectActivityObject);
        }
        return projectActivityList;

    }

    //Olav (s184195)
    //Formats the start date and end date for the given activity and puts it in the container
    private void formatActivityDates(JSONObject container, Activity activity){
        SimpleDate startDate = activity.getStartDate();
        SimpleDate endDate = activity.getEndDate();

        container.put("start week", startDate.getWeek());
        container.put("start year", startDate.getYear());
        container.put("end week", endDate.getWeek());
        container.put("end year", endDate.getYear());
    }
}