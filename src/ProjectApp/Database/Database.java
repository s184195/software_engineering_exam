package ProjectApp.Database;

import ProjectApp.ProjectManager;

//Olav (s184195)
public interface Database {
    void load(ProjectManager projectManager);
    void save();
}