package ProjectApp.Database;

import ProjectApp.Activities.ProjectActivity;
import ProjectApp.Employee;
import ProjectApp.InvalidOperationException;
import ProjectApp.Project;
import ProjectApp.ProjectManager;
import ProjectApp.Timings.SimpleDate;

import java.util.ArrayList;

//Olav (s184195)
public class DatabaseHardcoded implements Database {
    @Override
    public void load(ProjectManager projectManager) {
        mockEmployees(projectManager);
        mockProjects(projectManager);
        mockProjectActivities(projectManager);
        mockProjectActivityOverload(projectManager);
    }

    @Override
    public void save(){
        System.out.println("Saving! (Not really, this database is hardcoded)");
    }

    //Testing arraylist for employees
    private void mockEmployees(ProjectManager projectManager){
        String[] initials = {"JAHA","KARO","WIOL","DIJK","STRA","PRIM","KRUS","EINS"};
        ArrayList<Employee> employees = projectManager.getEmployees();
        if (employees.isEmpty()){
            for (int i = 0; i < initials.length; i++){
                try {
                    projectManager.addEmployee(initials[i]);
                } catch (InvalidOperationException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void mockProjects(ProjectManager projectManager){
        String[] projectNames = {"SQL Injections","Website for AIGA A/S", "Backend refactoring"};
        for (int i = 0; i < projectNames.length; i++){
            try {
                projectManager.createProject(projectNames[i]);
            } catch (InvalidOperationException e) {
                e.printStackTrace();
            }
        }
    }

    private void mockProjectActivities(ProjectManager projectManager){
        String[] projectActivityNames = {"Setup backend server","Establish internet connection","Refactor","Inject!"};
        int[] hours = {20,32,120,16};
        int[][] activityDates = {{1,2019},{4,2019},{3,2019},{4,2019},{5,2019},{10,2019},{10,2019},{13,2019}};
        Project firstProject = projectManager.getProjects().get(0);

        Employee sampleEmployee = projectManager.getEmployee("KARO");
        for (int i = 0; i < projectActivityNames.length; i++){
            SimpleDate startWeek = new SimpleDate(activityDates[2*i][1],activityDates[2*i][0]);
            SimpleDate endWeek  = new SimpleDate(activityDates[2*i + 1][1],activityDates[2*i + 1][0]);
            try {
                ProjectActivity createdActivity = firstProject.createProjectActivity(projectActivityNames[i],hours[i],startWeek, endWeek);
                sampleEmployee.getProjectActivities().add(createdActivity);
                createdActivity.addParticipant(sampleEmployee);
            } catch (InvalidOperationException e) {
                e.printStackTrace();
            }
        }
    }

    private void mockProjectActivityOverload(ProjectManager projectManager){
        Employee sampleEmployee = projectManager.getEmployee("PRIM");
        Project sampleProject = projectManager.getProject("Backend refactoring");

        SimpleDate startDate = new SimpleDate(2019,1);
        SimpleDate endDate = new SimpleDate(2019,5);

        for (int i = 0; i < 20; i++){
            try {
                ProjectActivity createdActivity = sampleProject.createProjectActivity(String.valueOf(i),(i + 2) * 4,startDate,endDate);
                sampleProject.addEmployeeToProjectActivity(createdActivity,sampleEmployee);
            } catch (InvalidOperationException e) {
                e.printStackTrace();
            }
        }
    }
}
