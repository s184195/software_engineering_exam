package ProjectApp.GraphicUI;

//Olav (s184195)
public enum SelectedScreen {
    LOGIN ("/loginScreen"),
    MAIN_DASHBOARD("/mainDashboard"),
    MAIN_OVERVIEW("/mainOverview"),
    PERSONAL_ACTIVITIES("/personalActivities"),
    REGISTER_TIME("/registerTime"),
    REQUEST_HELP("/requestHelp"),
    ALL_PROJECTS("/allProjects"),
    PROJECT_DASHBOARD("/projectDashboard"),
    PROJECT_OVERVIEW("/projectOverview"),
    PROJECT_ACTIVITY_OVERVIEW("/projectActivityOverview"),
    PROJECT_AVAILABLE_PARTICIPANTS("/availableParticipants");

    private String path;

    SelectedScreen(String path){
        this.path = path;
    }

    @Override
    public String toString(){
        return path + ".fxml";
    }
}
