package ProjectApp.GraphicUI.CustomCells;

import ProjectApp.Employee;

//Olav (s184195)
public class EmployeeAvailability {
    private Employee employee;
    private int[] weekStatus;

    public EmployeeAvailability(Employee employee){
        this.employee = employee;
    }

    public Employee getEmployee() {
        return employee;
    }

    public int[] getWeekStatus() {
        return weekStatus;
    }

    public void setWeekStatus(int[] weekStatus) {
        this.weekStatus = weekStatus;
    }
}
