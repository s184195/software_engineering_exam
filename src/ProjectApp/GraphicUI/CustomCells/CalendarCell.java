package ProjectApp.GraphicUI.CustomCells;

import ProjectApp.Activities.Activity;
import ProjectApp.Activities.ProjectActivity;
import ProjectApp.GraphicUI.Dialogs.CalendarPopup;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.io.IOException;
import java.util.ArrayList;

//Olav (s184195)
public class CalendarCell extends VBox {
    private boolean personalActive;
    private int weekNum;
    private ArrayList<Activity> weekActivites;

    @FXML
    private Text weekText;

    @FXML
    private Text projectText;

    @FXML
    private VBox projectBar;

    @FXML
    private VBox personalBar;

    public CalendarCell(int weekNum){
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/calendarCell.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.weekNum = weekNum;
        weekText.setText(String.valueOf(weekNum));
        projectBar.setVisible(false);
        personalBar.setVisible(false);

        autosize();
    }

    public void setWeekActivities(ArrayList<Activity> activities){
        weekActivites = activities;
        int activitiesInWeekCount = 0;

        for (Activity a: weekActivites){
            if (a instanceof ProjectActivity)
                activitiesInWeekCount++;
            else
                personalActive = true;
        }

        //Show the bar areas
        if (activitiesInWeekCount > 0){
            projectBar.setVisible(true);
            projectText.setText(String.valueOf(activitiesInWeekCount));
        } else
            projectBar.setVisible(false);

        personalBar.setVisible(personalActive);
    }

    @FXML
    public void onClick(){
        if (!weekActivites.isEmpty())
            new CalendarPopup(weekActivites,weekNum).showAndWait();
    }

    @FXML
    public void onHoverStart(){
        getStyleClass().add("calendarHover");
        if (projectBar.isVisible() || personalActive)
            setCursor(Cursor.HAND);
    }

    @FXML
    public void onHoverEnd(){
        getStyleClass().remove("calendarHover");
        setCursor(Cursor.DEFAULT);
    }
}
