package ProjectApp.GraphicUI.CustomCells;

import ProjectApp.Employee;
import javafx.scene.control.ListCell;

//Olav (s184195)
public class EmployeeCell extends ListCell<Employee> {
    @Override
    protected void updateItem(Employee item, boolean empty) {
        super.updateItem(item, empty);

        setGraphic(null);

        if (item == null || empty){
            setText(null);
        }
        else{
            setText(item.getInitials());
        }
    }
}
