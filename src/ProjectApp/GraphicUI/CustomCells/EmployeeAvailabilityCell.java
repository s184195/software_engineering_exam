package ProjectApp.GraphicUI.CustomCells;

import ProjectApp.Employee;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

import java.io.IOException;

public class EmployeeAvailabilityCell extends ListCell<EmployeeAvailability> {

    @FXML
    private AnchorPane container;

    @FXML
    private Text employeeName;

    @FXML
    private VBox canvasContainer;

    @FXML
    private Canvas canvas;
    private GraphicsContext gc;

    private FXMLLoader fxmlLoader;
    private ListView<EmployeeAvailability> parentListView;

    public EmployeeAvailabilityCell(ListView<EmployeeAvailability> parentListView){
        this.parentListView = parentListView;
    }

    @Override
    protected void updateItem(EmployeeAvailability item, boolean empty) {
        super.updateItem(item, empty);

        if (item == null || empty){
            setGraphic(null);
        }

        else{
            //It hasn't been instantiated before
            if (fxmlLoader == null){
                fxmlLoader = new FXMLLoader(getClass().getResource("/employeeAvailabilityCell.fxml"));
                fxmlLoader.setController(this);

                try {
                    fxmlLoader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            Employee employee = item.getEmployee();
            employeeName.setText(employee.getInitials());
            gc = canvas.getGraphicsContext2D();

            //The text is 40px wide with 8px indentation. The added 20px is to avoid bottom-scrollbar
            canvas.widthProperty().bind(parentListView.widthProperty().subtract(68));
            canvas.widthProperty().addListener(param -> drawAvailability(item.getWeekStatus()));

            setGraphic(container);
            drawAvailability(item.getWeekStatus());
        }
        setText(null);
    }

    private void drawAvailability(int[] weekStatus){
        if (weekStatus == null)
            return;

        int cellCount = weekStatus.length;
        double cellWidth = canvas.getWidth() / cellCount;
        double cellHeight = canvas.getHeight();

        gc.clearRect(0,0,canvas.getWidth(),cellHeight);
        for (int i = 0; i < cellCount; i++){
            if (Math.abs(weekStatus[i]) - 1 >= Employee.MAX_PROJECTS)
                gc.setFill(Color.RED);
            else if (weekStatus[i] < 0)
                gc.setFill(Color.PURPLE);
            else
                gc.setFill(Color.GREEN);

            gc.fillRect(cellWidth * i + 5, 0, cellWidth - 5, cellHeight);
        }
    }
}
