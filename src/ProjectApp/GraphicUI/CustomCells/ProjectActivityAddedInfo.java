package ProjectApp.GraphicUI.CustomCells;

import ProjectApp.Activities.ProjectActivity;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ListCell;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

import java.io.IOException;

//Olav (s184195)
public class ProjectActivityAddedInfo extends ListCell<ProjectActivity> {
    @FXML
    private AnchorPane container;

    @FXML
    private Text activityNameText;

    @FXML
    private Text hoursRemainingText;

    @FXML
    private Text startDateText;

    @FXML
    private Text endDateText;

    private FXMLLoader fxmlLoader;

    @Override
    protected void updateItem(ProjectActivity item, boolean empty) {
        super.updateItem(item, empty);

        setText(null);
        if (item == null || empty){
            setGraphic(null);
        } else{
            //It hasn't been instantiated before
            if (fxmlLoader == null){
                fxmlLoader = new FXMLLoader(getClass().getResource("/projectActivityAddedInfo.fxml"));
                fxmlLoader.setController(this);
                try {
                    fxmlLoader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            activityNameText.setText(item.getName());
            hoursRemainingText.setText(String.valueOf(item.getHoursAllocated() - item.getHours()));
            startDateText.setText(item.getStartDate().toString());
            endDateText.setText(item.getEndDate().toString());

            setGraphic(container);
        }
    }
}
