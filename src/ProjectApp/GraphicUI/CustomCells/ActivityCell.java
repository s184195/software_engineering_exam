package ProjectApp.GraphicUI.CustomCells;

import ProjectApp.Activities.Activity;
import ProjectApp.Activities.ProjectActivity;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ListCell;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

import java.io.IOException;

//Olav (s184195)
//https://github.com/turais/TuraisJavaFxExamples/blob/master/src/de/turais/samples/StudentListViewCell.java
public class ActivityCell extends ListCell<Activity>{
    @FXML
    private AnchorPane container;

    @FXML
    private Text activityText;

    @FXML
    private Text projectText;

    @FXML
    private Text startText;

    @FXML
    private Text endText;

    private FXMLLoader fxmlLoader;

    @Override
    protected void updateItem(Activity item, boolean empty) {
        super.updateItem(item, empty);

        setText(null);

        if (item == null || empty){
            setGraphic(null);
        }
        else{
            //It hasn't been instantiated before
            if (fxmlLoader == null){
                fxmlLoader = new FXMLLoader(getClass().getResource("/activityCell.fxml"));
                fxmlLoader.setController(this);

                try {
                    fxmlLoader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            activityText.setText(item.getName());
            startText.setText(item.getStartDate().toString());
            endText.setText(item.getEndDate().toString());

            if (item instanceof ProjectActivity)
                projectText.setText(((ProjectActivity) item).getParentProject().getProjectName());
            else{
                projectText.setManaged(false);
                projectText.setVisible(false);
            }

            setGraphic(container);
        }
    }
}
