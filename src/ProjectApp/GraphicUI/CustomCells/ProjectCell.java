package ProjectApp.GraphicUI.CustomCells;

import ProjectApp.Employee;
import ProjectApp.Project;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ListCell;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

import java.io.IOException;

//Olav (s184195)
public class ProjectCell extends ListCell<Project> {
    @FXML
    private AnchorPane container;

    @FXML
    private Text projectName;

    @FXML
    private Text projectLeaderName;

    private FXMLLoader fxmlLoader;

    @Override
    protected void updateItem(Project item, boolean empty) {
        super.updateItem(item, empty);

        if (item == null || empty){
            setGraphic(null);
        }
        else{
            //It hasn't been instantiated before
            if (fxmlLoader == null){
                fxmlLoader = new FXMLLoader(getClass().getResource("/projectCell.fxml"));
                fxmlLoader.setController(this);

                try {
                    fxmlLoader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            String projectString = item.getProjectName();
            if (projectString == null)
                projectString = "";
            projectName.setText(projectString + " #"+ item.getId());

            Employee projectLeader = item.getProjectLeader();
            if (projectLeader != null)
                projectLeaderName.setText(projectLeader.getInitials());
            else
                projectLeaderName.setText("No project leader assigned");
            setGraphic(container);
        }
        setText(null);
    }
}
