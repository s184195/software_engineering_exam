package ProjectApp.GraphicUI;

import ProjectApp.Timings.SimpleDate;
import javafx.collections.ObservableList;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;

//Olav (s184195)
public class HourChart {
    private LineChart<String, Number> lineChart;

    public HourChart(LineChart<String, Number> lineChart, String title, String xLabel, String yLabel){
        this.lineChart = lineChart;
        lineChart.setTitle(title);
        lineChart.getXAxis().setLabel(xLabel);
        lineChart.getYAxis().setLabel(yLabel);
    }

    public void setRegisteredHours(double[] registeredHours, SimpleDate startDate){
        XYChart.Series<String,Number> hourSeries =  new XYChart.Series<>();
        ObservableList<XYChart.Data<String,Number>> hourData = hourSeries.getData();

        int totalHours = 0;
        SimpleDate formattedDate = new SimpleDate(startDate.getYear(),startDate.getWeek());

        for (int i = 0; i < registeredHours.length; i++){
            totalHours += registeredHours[i];
            hourData.add(new XYChart.Data<>(formattedDate.toString(),totalHours));

            if (formattedDate.getWeek() == 52)
                formattedDate.setWeek(1);
            else
                formattedDate.setWeek(formattedDate.getWeek() + 1);
        }
        lineChart.getData().setAll(hourSeries);
    }
}
