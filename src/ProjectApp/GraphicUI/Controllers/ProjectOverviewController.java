package ProjectApp.GraphicUI.Controllers;

import ProjectApp.GraphicUI.HourChart;
import ProjectApp.Project;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.text.Text;

//Olav (s184195)
public class ProjectOverviewController extends BaseController{
    private Project selectedProject;

    @FXML
    private Text nParticipantText;
    @FXML
    private Text participantText;

    @FXML
    private Text nActivityText;

    @FXML
    private Text activityText;

    @FXML
    private LineChart<String,Number> progressChart;

    @Override
    public void launch() {
        selectedProject = getProjectManager().getSelectedProject();

        int participantCount = selectedProject.getParticipants().size();
        nParticipantText.setText(String.valueOf(participantCount));
        if (participantCount == 1)
            participantText.setText("participant");

        int activityCount = selectedProject.getActivities().size();
        nActivityText.setText(String.valueOf(activityCount));
        if (activityCount == 1)
            activityText.setText("ongoing activity");
        loadGraph();
    }

    private void loadGraph(){
        if (selectedProject.getActivities().size() == 0)
            return;


        HourChart registeredHoursChart = new HourChart(progressChart, "Progress","Dates","Total hours");
        registeredHoursChart.setRegisteredHours(selectedProject.computeProjectProgress(),selectedProject.getMinDate());
    }
}
