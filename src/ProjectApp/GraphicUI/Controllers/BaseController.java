package ProjectApp.GraphicUI.Controllers;

import ProjectApp.GraphicUI.SelectedScreen;
import ProjectApp.ProjectManager;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.Pane;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

//Olav (s184195)
public abstract class BaseController {
    private ProjectManager projectManager;
    private BaseController masterController;
    private Pane rootPane;

    //GETTERS AND SETTERS
    public ProjectManager getProjectManager() {
        return projectManager;
    }

    public void setProjectManager(ProjectManager projectManager) {
        this.projectManager = projectManager;
    }

    public BaseController getMasterController() {
        return masterController;
    }

    public void setMasterController(BaseController masterController) {
        this.masterController = masterController;
    }


    public Pane getRootPane() {
        return rootPane;
    }

    public void setRootPane(Pane rootPane) {
        this.rootPane = rootPane;
    }

    //METHODS

    //Function is called after references to ProjectManager is set up
    public abstract void launch();

    public void setupDashboard(Pane rootPane){
        setRootPane(rootPane);

        rootPane.widthProperty().addListener((observable, oldValue, newValue) -> {
            rescaleChildren();
        });
        rootPane.heightProperty().addListener((observable, oldValue, newValue) -> {
            rescaleChildren();
        });
    }
    //Rescales the content inside the root-pane
    public void rescaleChildren(){
        if (rootPane != null){
            double width = rootPane.getWidth();
            double height = rootPane.getHeight();

            //Ensures no weird scaling on launch
            if (width < 1 || height < 1)
                return;

            //Update each child
            for (Node n: rootPane.getChildren()){
                Pane nPane = (Pane)n;
                nPane.setPrefWidth(width);
                nPane.setPrefHeight(height);
            }
        }
    }

    //Load a new menu-screen over this
    public BaseController loadScreenInPlace(String fxmlUrl){
      return loadScreen(fxmlUrl,true);
    }

    //Load a new menu-screen over this
    public BaseController loadScreenInPlace(SelectedScreen fxmlUrl){
        return loadScreen(fxmlUrl.toString(),true);
    }

    //Load new screen as a child of this one
    public BaseController loadScreenAsChild(String fxmlUrl){
        return loadScreen(fxmlUrl,false);
    }

    //Load new screen as a child of this one
    public BaseController loadScreenAsChild(SelectedScreen fxmlUrl){
        return loadScreen(fxmlUrl.toString(),false);
    }

    //Helper method for loading screen
    private BaseController loadScreen(String fxmlUrl, boolean inplace){
        BaseController controller = null;
        try {
            //Load given file
            FXMLLoader loader = new FXMLLoader(getClass().getResource(fxmlUrl));
            Parent root = loader.load();

            controller = loader.getController();

            //Substitute the calling screen with the new
            if (inplace){
                controller.setMasterController(masterController);
                masterController.getRootPane().getChildren().setAll(root);
                masterController.rescaleChildren();
            }
            //Add the new screen as a child of the caller
            else{
                controller.setMasterController(this);
                rootPane.getChildren().setAll(root);
                rescaleChildren();
            }

            //File has been loaded - update content
            controller.setProjectManager(projectManager);
            controller.launch();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return controller;
    }
}
