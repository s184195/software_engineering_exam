package ProjectApp.GraphicUI.Controllers;

import ProjectApp.Employee;
import ProjectApp.GraphicUI.CustomCells.EmployeeAvailability;
import ProjectApp.GraphicUI.CustomCells.EmployeeAvailabilityCell;
import ProjectApp.Timings.SimpleDate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;

import java.time.LocalDate;

//Olav (s184195)
public class AvailableParticipantController extends BaseController{
    private ObservableList<EmployeeAvailability> participants;
    private static final int MIN_CELL_WIDTH = 20;

    @FXML
    private AnchorPane root;

    @FXML
    private ListView<EmployeeAvailability> employeeListView;

    @FXML
    private DatePicker startDatePicker;

    @FXML
    private DatePicker endDatePicker;

    @FXML
    private Canvas weekCanvas;
    private GraphicsContext gc;

    @Override
    public void launch() {
        participants = FXCollections.observableArrayList();
        for (Employee e: getProjectManager().getSelectedProject().getParticipants())
            participants.add(new EmployeeAvailability(e));

        employeeListView.setCellFactory(param -> new EmployeeAvailabilityCell(employeeListView));
        employeeListView.setItems(participants);

        startDatePicker.valueProperty().addListener((observable, oldValue , newValue) -> updateAvailableParticipants());
        endDatePicker.valueProperty().addListener((observable, oldValue , newValue) -> updateAvailableParticipants());

        gc = weekCanvas.getGraphicsContext2D();
        gc.setFill(Color.BLACK);


        root.widthProperty().addListener(((observable, oldValue, newValue) -> {
            weekCanvas.setWidth((double) newValue - 73);
        }));

        weekCanvas.widthProperty().addListener(((observable, oldValue, newValue) -> {
            drawWeeks();
        }));

    }

    private void updateAvailableParticipants(){
        LocalDate startDateLocal = startDatePicker.getValue();
        LocalDate endDateLocal = endDatePicker.getValue();

        if (validDates(startDateLocal, endDateLocal)){
            drawWeeks();
            for (EmployeeAvailability e: participants){
                e.setWeekStatus(e.getEmployee().occupiedWeeks(new SimpleDate(startDateLocal),new SimpleDate(endDateLocal)));
            }
            //Update listview
            employeeListView.refresh();
        }
    }

    private void drawWeeks(){

        LocalDate startDateLocal = startDatePicker.getValue();
        LocalDate endDateLocal = endDatePicker.getValue();

        if (validDates(startDateLocal, endDateLocal)){
            //Clear from last iteration
            gc.clearRect(0,0,weekCanvas.getWidth(),weekCanvas.getHeight());
            SimpleDate startDate = new SimpleDate(startDateLocal);
            SimpleDate endDate = new SimpleDate(endDateLocal);

            int weekDiff = startDate.compareTo(endDate) + 1;

            //Ensure the cells are big enough to fit the text
            double cellWidth = weekCanvas.getWidth() / weekDiff;
            int iStep = (int) Math.ceil(MIN_CELL_WIDTH / cellWidth);
            cellWidth*= iStep;

            //Draw each cell
            for (int i = 0; i <= weekDiff; i+=iStep) {
                int weekNumber =   startDate.getWeek() + i*iStep;
                if (weekNumber >= 53)
                    weekNumber -= 52;
                gc.fillText(String.valueOf(weekNumber), i * cellWidth + 5, 30);
            }
        }
    }

    private boolean validDates(LocalDate startDateLocal, LocalDate endDateLocal){
        if (startDateLocal != null && endDateLocal != null){
            SimpleDate startDate = new SimpleDate(startDateLocal);
            SimpleDate endDate = new SimpleDate(endDateLocal);
            return startDate.compareTo(endDate) >= 0;
        }
        return false;
    }
}
