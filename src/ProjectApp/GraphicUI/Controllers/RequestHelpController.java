package ProjectApp.GraphicUI.Controllers;

import ProjectApp.Activities.Activity;
import ProjectApp.Activities.ProjectActivity;
import ProjectApp.Employee;
import ProjectApp.GraphicUI.CustomCells.ActivityCell;
import ProjectApp.GraphicUI.CustomCells.EmployeeCell;
import ProjectApp.GraphicUI.Dialogs.ErrorDialog;
import ProjectApp.InvalidOperationException;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

import java.util.ArrayList;

//Olav (s184195)
public class RequestHelpController extends BaseController {
    private ProjectActivity selectedActivity;
    private Employee selectedEmployee;
    private Employee employeeNoSelection = new Employee("None selected");

    @FXML
    private ListView<Activity> projectActivityListView;

    @FXML
    private ComboBox<Employee> requestedEmployeeSelector;

    @FXML
    private AnchorPane selectedActivityArea;

    @FXML
    private Text selectedActivityText;

    @FXML
    private Button registerHelpButton;

    @Override
    public void launch() {
        projectActivityListView.setCellFactory(param -> new ActivityCell());
        projectActivityListView.setItems(FXCollections.observableArrayList(getProjectManager()
                .getEmployeeUsingSystem()
                .getProjectActivities()));

        projectActivityListView.getSelectionModel().selectedItemProperty().addListener(((observable, oldValue, newValue) -> {
            selectedActivity = (ProjectActivity)newValue;
            prepareSelectedActivityArea(true);
        }));

        requestedEmployeeSelector.setButtonCell(new EmployeeCell());
        requestedEmployeeSelector.setCellFactory(param -> new EmployeeCell());
        requestedEmployeeSelector.getSelectionModel().selectedItemProperty().addListener(((observable, oldValue, newValue) -> {
            if (newValue.equals(employeeNoSelection))
                selectedEmployee = null;
            else
                selectedEmployee = newValue;
            updateRegisterButton();
        }));
        prepareSelectedActivityArea(false);
    }

    @FXML
    public void requestHelp(){
        try {
            getProjectManager().requestHelp(selectedEmployee,selectedActivity);
            //Update view
            prepareSelectedActivityArea(true);
        } catch (InvalidOperationException e) {
            new ErrorDialog(e.getMessage()).showAndWait();
        }
    }

    private void updateRegisterButton(){
        registerHelpButton.setDisable(selectedEmployee == null);
    }

    private void prepareSelectedActivityArea(boolean show){
        if (show && selectedActivity != null){
            selectedActivityText.setText(selectedActivity.getName());

            //Get available employees to add
            ArrayList<Employee> availableEmployees = getProjectManager()
                    .getAvailableEmployees(selectedActivity.getStartDate(),selectedActivity.getEndDate());
            for (Employee e: selectedActivity.getParticipants())
                availableEmployees.remove(e);

            availableEmployees.add(0,employeeNoSelection);

            requestedEmployeeSelector.getItems().setAll(availableEmployees);
            requestedEmployeeSelector.getSelectionModel().select(employeeNoSelection);

        } else {
            selectedActivityText.setText("");
        }
        selectedActivityArea.setVisible(show);
    }
}