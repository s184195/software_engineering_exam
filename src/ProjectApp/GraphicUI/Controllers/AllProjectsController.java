package ProjectApp.GraphicUI.Controllers;

import ProjectApp.Employee;
import ProjectApp.GraphicUI.CustomCells.EmployeeCell;
import ProjectApp.GraphicUI.CustomCells.ProjectCell;
import ProjectApp.GraphicUI.Dialogs.ErrorDialog;
import ProjectApp.GraphicUI.SelectedScreen;
import ProjectApp.InvalidOperationException;
import ProjectApp.Project;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Optional;

//Olav (s184195)
public class AllProjectsController extends BaseController{
    private boolean preparingProjectArea;
    private Project selectedProject;
    private ObservableList<Project> projectObservableList;
    private ObservableList<Employee> employeeObservableList;
    private Employee employeeNoSelection = new Employee("None selected");

    @FXML
    private ListView<Project> projectListView;

    @FXML
    private Text projectTitle;

    @FXML
    private ComboBox<Employee> projectLeaderSelection;

    @FXML
    private TextField projectNameInput;

    @FXML
    private AnchorPane projectArea;

    @FXML
    private Button openProjectButton;

    @Override
    public void launch() {
        projectTitle.setText("");
        projectArea.setVisible(false);

        //Get values from Project Manager
        projectObservableList = FXCollections.observableList(getProjectManager().getProjects());
        employeeObservableList = FXCollections.observableList((ArrayList<Employee>)getProjectManager().getEmployees().clone());

        employeeObservableList.add(0, employeeNoSelection);

        //Set custom adapter for projects
        projectListView.setItems(projectObservableList);
        projectListView.setCellFactory(projectListView -> new ProjectCell());

        projectLeaderSelection.setItems(employeeObservableList);
        projectLeaderSelection.setCellFactory(param -> new EmployeeCell());
        projectLeaderSelection.setButtonCell(new EmployeeCell());

        //Eventhandler
        projectListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->{
            preparingProjectArea = true;

            selectedProject = newValue;
            getProjectManager().setSelectedProject(selectedProject);

            projectTitle.setText(selectedProject.toString());
            Employee projectLeader = selectedProject.getProjectLeader();

            projectNameInput.setText(selectedProject.getProjectName());

            //Does the project already have a project leader?
            projectLeaderSetup();
            if (projectLeader != null){
                projectLeaderSelection.setValue(projectLeader);
                projectLeaderSelection.setDisable(true);
            } else{
                projectLeaderSelection.setValue(employeeNoSelection);
                projectLeaderSelection.setDisable(false);
            }

            projectArea.setVisible(true);
            preparingProjectArea = false;
        });

        //Listener for project leader selection
        projectLeaderSelection.getSelectionModel().selectedItemProperty().addListener(((observable, oldValue, newValue) -> {
            if (!newValue.equals(employeeNoSelection)){
                //Avoid re-setting project leader
                if (!newValue.equals(selectedProject.getProjectLeader())){
                    try {
                        getProjectManager().appointProjectLeader(selectedProject,newValue);
                        projectListView.refresh();
                        projectLeaderSetup();
                    } catch (InvalidOperationException e) {
                        e.printStackTrace();
                    }
                }
                projectLeaderSelection.setDisable(true);
            }
        }));

        projectNameInput.textProperty().addListener(((observable, oldValue, newValue) -> {
            if (!preparingProjectArea){
                selectedProject.setProjectName(newValue);
                projectTitle.setText(selectedProject.toString());
                projectListView.refresh();
            }
        }));
    }

    //Is the current employee the project leader of the given project?
    //If so, show the open project button
    private void projectLeaderSetup(){
        openProjectButton.setVisible(getProjectManager().getEmployeeUsingSystem().equals(selectedProject.getProjectLeader()));
    }

    @FXML
    public void openProject(){
        getMasterController().loadScreenInPlace(SelectedScreen.PROJECT_DASHBOARD);
    }

    @FXML
    public void createNewProject(){
        //https://stackoverflow.com/questions/37414441/javafx-new-custom-pop-out-window
        //Setup
        Dialog<Pair<String, Integer>> dialog = createProjectDialog();

        Optional<Pair<String,Integer>> result = dialog.showAndWait();
        result.ifPresent(projectData -> {
            try {
                Project createdProject = getProjectManager().createProject(projectData.getKey());
                int projectLeaderIndex = projectData.getValue();

                if (projectLeaderIndex > 0){
                    getProjectManager().appointProjectLeader(createdProject,getProjectManager().getEmployees().get(projectLeaderIndex - 1));
                }

                //Hack for getting scrollbar
                projectListView.getItems().add(null);
                projectListView.getItems().remove(projectObservableList.size() - 1);

                projectListView.refresh();
            } catch (InvalidOperationException e) {
                ErrorDialog errorDialog = new ErrorDialog(e.getMessage());
                errorDialog.showAndWait();
            }
        });
    }

    private Dialog createProjectDialog(){
        Dialog<Pair<String, Integer>> dialog = new Dialog<>();
        dialog.setTitle("Create new project");
        dialog.setHeaderText("Both project name and project leader are optional");

        //Confirmation and cancel buttons
        ButtonType createProjectButton = new ButtonType("Create", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(createProjectButton, ButtonType.CANCEL);

        TextField projectName = new TextField();
        projectName.setPromptText("Project name");

        ComboBox<Employee> projectLeaderSelection = new ComboBox<>(employeeObservableList);
        projectLeaderSelection.setCellFactory(param -> new EmployeeCell());
        projectLeaderSelection.setButtonCell(new EmployeeCell());
        projectLeaderSelection.setVisibleRowCount(10);
        projectLeaderSelection.getSelectionModel().select(0);


        //Grid for elements
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        //Add elements to the grid
        grid.add(new Label("Project name:"), 0, 0);
        grid.add(projectName, 1, 0);
        grid.add(new Label("Project leader:"), 0, 1);
        grid.add(projectLeaderSelection, 1, 1);

        //Put grid in dialog
        dialog.getDialogPane().setContent(grid);

        dialog.getDialogPane().getStylesheets().add(getClass().getResource("/application.css").toString());
        //Returns given name and index of employee when "Create" button is pressed.
        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == createProjectButton){
                String projectNameInput = projectName.getText();
                if (projectNameInput.trim().isEmpty())
                    return new Pair<>(null, projectLeaderSelection.getSelectionModel().getSelectedIndex());
                return new Pair<>(projectNameInput, projectLeaderSelection.getSelectionModel().getSelectedIndex());
            }

            return null;
        });

        return dialog;
    }
}
