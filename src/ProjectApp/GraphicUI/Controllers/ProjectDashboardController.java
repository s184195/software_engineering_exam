package ProjectApp.GraphicUI.Controllers;

import ProjectApp.GraphicUI.SelectedScreen;
import ProjectApp.Project;
import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

//Olav (s184195)
public class ProjectDashboardController extends BaseController{
    private Project selectedProject;

    @FXML
    private AnchorPane rootPane;

    @FXML
    private Text projectTitle;

    @FXML
    private Text initialText;

    @Override
    public void launch() {
        setupDashboard(rootPane);

        selectedProject = getProjectManager().getSelectedProject();
        projectTitle.setText(selectedProject.toString());
        initialText.setText(getProjectManager().getEmployeeUsingSystem().getInitials());
        loadScreenAsChild(SelectedScreen.PROJECT_OVERVIEW);
    }

    @FXML
    public void openActivities(){
        loadScreenAsChild(SelectedScreen.PROJECT_ACTIVITY_OVERVIEW);
    }

    @FXML
    public void backToAllProjects(){
        BaseController controller = loadScreenInPlace(SelectedScreen.MAIN_DASHBOARD);
        controller.loadScreenAsChild(SelectedScreen.ALL_PROJECTS);
    }

    @FXML
    public void projectOverview(){
        loadScreenAsChild(SelectedScreen.PROJECT_OVERVIEW);
    }

    @FXML
    public void availableEmployees(){loadScreenAsChild(SelectedScreen.PROJECT_AVAILABLE_PARTICIPANTS);}
}
