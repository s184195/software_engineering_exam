package ProjectApp.GraphicUI.Controllers;

import ProjectApp.Activities.Activity;
import ProjectApp.Activities.PersonalActivity;
import ProjectApp.Employee;
import ProjectApp.GraphicUI.CustomCells.ActivityCell;
import ProjectApp.GraphicUI.Dialogs.ActivityDialog;
import ProjectApp.GraphicUI.Dialogs.ErrorDialog;
import ProjectApp.InvalidOperationException;
import ProjectApp.Timings.SimpleDate;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.util.Pair;

import java.time.LocalDate;
import java.util.Optional;

//Olav (s184195)
public class PersonalActivityController extends BaseController{
    private Employee selectedEmployee;
    private PersonalActivity selectedPersonalActivity;
    private boolean updatingViews;
    @FXML
    private ListView<Activity> personalActivityListView;

    @FXML
    private Text personalActivityTitle;

    @FXML
    private TextField titleInput;

    @FXML
    private DatePicker startDatePicker;

    @FXML
    private DatePicker endDatePicker;

    @FXML
    private AnchorPane selectedActivityArea;

    @Override
    public void launch() {
        selectedEmployee = getProjectManager().getEmployeeUsingSystem();
        personalActivityListView.setCellFactory(param -> new ActivityCell());
        personalActivityListView.setItems(FXCollections.observableArrayList(selectedEmployee.getPersonalActivities()));
        personalActivityListView.getSelectionModel().selectedItemProperty().addListener(((observable, oldValue, newValue) -> {
            selectedPersonalActivity = (PersonalActivity) newValue;
            prepareSelectedPersonalActivityArea(true);
        }));
        prepareSelectedPersonalActivityArea(false);

        startDatePicker.valueProperty().addListener(((observable, oldValue, newValue) -> {
            if (!updatingViews) {
                try {
                    getProjectManager().updateStartDateInActivity(selectedPersonalActivity,new SimpleDate(newValue));
                    personalActivityListView.refresh();
                } catch (InvalidOperationException e) {
                    new ErrorDialog(e.getMessage()).showAndWait();
                }
            }}));

        endDatePicker.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (!updatingViews) {
                try {
                    getProjectManager().updateEndDateInActivity(selectedPersonalActivity,new SimpleDate(newValue));
                    personalActivityListView.refresh();
                } catch (InvalidOperationException e) {
                    new ErrorDialog(e.getMessage()).showAndWait();
                }
            }});
    }

    @FXML
    public void registerPersonalActivity(){
        ActivityDialog dialog = new ActivityDialog(false);
        boolean finished = false;
        do{
            String errorMessage = "";

            Optional<Pair<Pair<String, LocalDate>, LocalDate>> result = dialog.showAndWait();
            if (result.isPresent()) {
                String activityName = result.get().getKey().getKey();

                //Test for proper name
                if (activityName != null && !activityName.trim().isEmpty()){
                    LocalDate startDate = result.get().getKey().getValue();
                    LocalDate endDate = result.get().getValue();

                    if (startDate != null && endDate != null){
                        try {
                            PersonalActivity activity = getProjectManager().makePersonalActivityForEmployee(getProjectManager()
                                    .getEmployeeUsingSystem(),activityName,new SimpleDate(startDate),new SimpleDate(endDate));
                            finished = true;
                            personalActivityListView.getItems().add(activity);
                            personalActivityListView.refresh();
                        } catch (InvalidOperationException e) {
                            errorMessage = e.getMessage();
                        }
                    } else
                        errorMessage = "Dates are in invalid format";
                } else
                    errorMessage = "Name must be non-empty";
                if (!errorMessage.isEmpty())
                    new ErrorDialog(errorMessage).showAndWait();
            } else finished = true;
        } while (!finished);
    }

    @FXML
    public void removeSelectedActivity(){
        selectedEmployee.getPersonalActivities().remove(selectedPersonalActivity);
        personalActivityListView.getItems().remove(selectedPersonalActivity);

        personalActivityListView.getSelectionModel().select(null);
        personalActivityListView.refresh();
        prepareSelectedPersonalActivityArea(false);
    }

    private void prepareSelectedPersonalActivityArea(boolean show){
        if (show && selectedPersonalActivity != null){
            updatingViews = true;
            selectedActivityArea.setVisible(true);
            personalActivityTitle.setText(selectedPersonalActivity.getName());

            titleInput.setText(selectedPersonalActivity.getName());
            startDatePicker.setValue(selectedPersonalActivity.getStartDate().toLocalDate());
            endDatePicker.setValue(selectedPersonalActivity.getEndDate().toLocalDate());
            updatingViews = false;
        } else {
            selectedActivityArea.setVisible(false);
            personalActivityTitle.setText("");
        }
    }
}