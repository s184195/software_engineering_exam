package ProjectApp.GraphicUI.Controllers;

import ProjectApp.Activities.ProjectActivity;
import ProjectApp.Employee;
import ProjectApp.GraphicUI.CustomCells.EmployeeCell;
import ProjectApp.GraphicUI.CustomCells.ProjectActivityAddedInfo;
import ProjectApp.GraphicUI.Dialogs.ActivityDialog;
import ProjectApp.GraphicUI.Dialogs.ErrorDialog;
import ProjectApp.GraphicUI.Dialogs.ParticipantDialog;
import ProjectApp.GraphicUI.HourChart;
import ProjectApp.InvalidOperationException;
import ProjectApp.Project;
import ProjectApp.Timings.SimpleDate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.util.Pair;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;

//Olav (s184195)
public class ProjectActivityOverviewController extends BaseController {
    private Project selectedProject;
    private ProjectActivity selectedActivity;

    private final int noArrowOffset = 25; //Offset to avoid arrow
    private ObservableList<ProjectActivity> projectActivityObservableList;
    private ObservableList<Employee> participants;

    private boolean changingViews;

    @FXML
    private ListView<ProjectActivity> projectActivityListView;

    @FXML
    private ListView<Employee> participantListView;

    @FXML
    private VBox selectedActivityArea;

    @FXML
    private TabPane tabPane;

    @FXML
    private Text activityNameText;

    @FXML
    private TextField activityNameField;

    @FXML
    private DatePicker startDatePicker;

    @FXML
    private DatePicker endDatePicker;

    @FXML
    private TextField hoursNeededInput;

    @FXML
    private LineChart<String,Number> activityProgress;

    @Override
    public void launch() {
        selectedProject = getProjectManager().getSelectedProject();

        selectedActivityArea.setVisible(false);
        tabPane.widthProperty().addListener((observable, oldValue, newValue) -> {
            tabPane.setTabMinWidth(tabPane.getWidth() / 2 - noArrowOffset);
            tabPane.setPrefWidth(tabPane.getWidth()/2);
            tabPane.setTabMaxWidth(tabPane.getWidth() / 2);
        });

        projectActivityObservableList = FXCollections.observableList(selectedProject.getActivities());
        projectActivityListView.setCellFactory(param -> new ProjectActivityAddedInfo());
        projectActivityListView.setItems(projectActivityObservableList);

        projectActivityListView.getSelectionModel().selectedItemProperty().addListener(((observable, oldValue, newValue) -> {
            selectedActivityArea.setVisible(true);
            selectedActivity = newValue;
            prepareSelectedActivityArea(selectedActivity);
        }));

        activityNameField.textProperty().addListener((observable, oldValue, newValue) -> {
            updateProjectActivity(selectedActivity);
        });

        startDatePicker.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (!changingViews) {
                try {
                    getProjectManager().updateStartDateInActivity(selectedActivity,new SimpleDate(newValue));
                } catch (InvalidOperationException e) {
                    new ErrorDialog(e.getMessage()).showAndWait();
                }
            }
            projectActivityListView.refresh();
            prepareSelectedActivityArea(selectedActivity);
        });

        endDatePicker.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (!changingViews) {
                try {
                    getProjectManager().updateEndDateInActivity(selectedActivity,new SimpleDate(newValue));
                } catch (InvalidOperationException e) {
                    new ErrorDialog(e.getMessage()).showAndWait();
                }
            }
            projectActivityListView.refresh();
            prepareSelectedActivityArea(selectedActivity);
        });

        hoursNeededInput.textProperty().addListener((observable, oldValue, newValue) -> updateProjectActivity(selectedActivity));
        activityProgress.getXAxis().setLabel("Date");
        activityProgress.getXAxis().setLabel("Hours");
    }

    private void prepareSelectedActivityArea(ProjectActivity projectActivity){
        if (projectActivity == null)
            return;

        changingViews = true;

        SimpleDate startDate = selectedActivity.getStartDate();

        //Set text for header
        activityNameText.setText(projectActivity.getName());
        activityNameField.setText(projectActivity.getName());

        //Input activity dates
        startDatePicker.setValue(startDate.toLocalDate());
        endDatePicker.setValue(projectActivity.getEndDate().toLocalDate());

        //Add the participants
        participants = FXCollections.observableList(projectActivity.getParticipants());
        participantListView.setCellFactory(param -> new EmployeeCell());
        participantListView.setItems(participants);
        participantListView.refresh();

        //Show the hours needed for the activities
        hoursNeededInput.setText(String.valueOf(projectActivity.getHoursAllocated()));

        //Chart the progress for the selected activity
        HourChart progressChart = new HourChart(activityProgress, "Hours spent","Dates","Total hours");
        progressChart.setRegisteredHours(selectedActivity.getWeeklyProgress(),startDate);
        changingViews = false;
    }

    private void updateProjectActivity(ProjectActivity projectActivity){
        if (changingViews)
            return;

        String activityName = activityNameField.getText();

        //Test for valid activity-name
        try {
            getProjectManager().updateProjectActivityName(selectedProject,selectedActivity,activityName);
            activityNameText.setText(activityName);
        } catch (InvalidOperationException e) {
            new ErrorDialog(e.getMessage()).showAndWait();
        }

        //Valid hour input
        try{
            double hours = Double.valueOf(hoursNeededInput.getText());
            projectActivity.setHoursAllocated(hours);
        } catch (NumberFormatException e){}
        catch (InvalidOperationException e) {
            new ErrorDialog(e.getMessage()).showAndWait();
        }

        //Update listview
        projectActivityListView.refresh();
    }

    @FXML
    public void addParticipantToActivity(){
        ArrayList<Employee> availableEmployees = getProjectManager().getAvailableEmployees(selectedActivity.getStartDate(),selectedActivity.getEndDate());

        //Remove employee if they're already in the activity
        for (Employee e: selectedActivity.getParticipants())
            availableEmployees.remove(e);

        ParticipantDialog dialog = new ParticipantDialog(availableEmployees);
        boolean finished = false;

        do {
            Optional<Employee> result = dialog.showAndWait();
            if (result.isPresent()){
                try {
                    getProjectManager().addEmployeeToProjectActivity(selectedProject,selectedActivity,result.get());
                    finished = true;

                    //Refresh participantListview
                    participantListView.getItems().add(null);
                    participantListView.getItems().remove(participants.size() - 1);
                    participantListView.refresh();
                } catch (InvalidOperationException e) {
                    e.printStackTrace();
                }
            } else finished = true;
        } while (!finished);
    }

    @FXML
    public void createNewActivity(){
        ActivityDialog dialog = new ActivityDialog(true);

        //Lambda has to use final?
        final boolean[] finished = {true};
        do {
            Optional<Pair<Pair<String,LocalDate>,Pair<LocalDate,String>>> result = dialog.showAndWait();
            if (result.isPresent()) result.ifPresent(activityData -> {
                String errorMessage = "";
                finished[0] = false;

                String activityName = activityData.getKey().getKey();
                if (isProjectActivityNameUnique(activityName)) {
                    //Get dates
                    LocalDate startDateAsLocal = activityData.getKey().getValue();
                    LocalDate endDateAsLocal = activityData.getValue().getKey();

                    //Test for actual dates
                    if (startDateAsLocal != null && endDateAsLocal != null) {
                        String hourString = activityData.getValue().getValue();
                        try {
                            double hours = Double.parseDouble(hourString);
                            if (hours <= 0)
                                throw new InvalidOperationException("Hours must be greater than 0");

                            //Create new activity in project
                            getProjectManager().createActivityInProject(selectedProject, activityName, hours, new SimpleDate(startDateAsLocal), new SimpleDate(endDateAsLocal));

                            //All fields were valid
                            finished[0] = true;

                            projectActivityListView.getItems().add(null);
                            projectActivityListView.getItems().remove(projectActivityListView.getItems().size() - 1);

                            projectActivityListView.refresh();

                        } catch (NumberFormatException e) {
                            errorMessage = "Hours were in invalid format";
                        } catch (InvalidOperationException e) {
                            errorMessage = e.getMessage();
                        }
                    } else
                        errorMessage = "Dates are in invalid format";
                } else
                    errorMessage = "Name must be non-empty and unique";

                //Show the error to the user
                if (!errorMessage.isEmpty()) new ErrorDialog(errorMessage).showAndWait();});
            else finished[0] = true;
        } while (!finished[0]);

    }

    private boolean isProjectActivityNameUnique(String name){
        if (name == null)
            return false;
        name = name.trim();

        if (name.isEmpty())
            return false;

        for (ProjectActivity projectActivity :selectedProject.getActivities())
            if (projectActivity.getName().equals(name))
                return false;
        return true;
    }
}
