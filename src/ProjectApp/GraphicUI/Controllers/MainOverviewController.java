package ProjectApp.GraphicUI.Controllers;

import ProjectApp.Activities.Activity;
import ProjectApp.GraphicUI.CustomCells.CalendarCell;
import ProjectApp.Timings.SimpleDate;
import javafx.fxml.FXML;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

import java.util.ArrayList;

public class MainOverviewController extends BaseController{
    private final int WEEKS_IN_YEAR = 52;
    private int year = 2019;
    private CalendarCell[] calendarCells = new CalendarCell[WEEKS_IN_YEAR];

    @FXML
    private GridPane weekGrid;

    @FXML
    private Text yearText;

    @Override
    public void launch() {
        for (int i = 0; i < WEEKS_IN_YEAR; i++){
            calendarCells[i] = new CalendarCell(i + 1);
            weekGrid.add(calendarCells[i], i % 8, i / 8);
        }
        prepareCalendar(year);
    }

    private void prepareCalendar(int year){
        yearText.setText(String.valueOf(year));

        ArrayList[] weeklyActivities = new ArrayList[WEEKS_IN_YEAR];
        for (int i = 0; i < WEEKS_IN_YEAR; i++)
            weeklyActivities[i] = new ArrayList<Activity>();

        //Dates for comparisons
        SimpleDate startWeekOfYear = new SimpleDate(year,1);
        SimpleDate endWeekOfYear = new SimpleDate(year,WEEKS_IN_YEAR);

        for (Activity a: getProjectManager().getEmployeeUsingSystem().getActivities()){
            int startIndex = Math.max(0,startWeekOfYear.compareTo(a.getStartDate()));
            int endIndex = Math.min(WEEKS_IN_YEAR,WEEKS_IN_YEAR + endWeekOfYear.compareTo(a.getEndDate()));

            for (int i = startIndex; i < endIndex; i++)
                weeklyActivities[i].add(a);
        }

        //Apply the counted projects to the cells
        for (int i = 0; i < WEEKS_IN_YEAR; i++)
            calendarCells[i].setWeekActivities(weeklyActivities[i]);
    }

    @FXML
    public void increaseYear(){
        prepareCalendar(++year);
    }

    @FXML
    public void decreaseYear(){
        prepareCalendar(--year);
    }
}