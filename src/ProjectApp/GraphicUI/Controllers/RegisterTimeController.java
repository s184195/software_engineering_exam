package ProjectApp.GraphicUI.Controllers;

import ProjectApp.Activities.Activity;
import ProjectApp.Activities.ProjectActivity;
import ProjectApp.Activities.Timelog;
import ProjectApp.GraphicUI.CustomCells.ActivityCell;
import ProjectApp.GraphicUI.Dialogs.ErrorDialog;
import ProjectApp.InvalidOperationException;
import ProjectApp.Timings.SimpleDate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

import java.time.LocalDate;
import java.util.ArrayList;

//Olav (s184195)
public class RegisterTimeController extends BaseController{
    private ProjectActivity selectedActivity;

    @FXML
    AnchorPane registerArea;

    @FXML
    DatePicker weekInput;

    @FXML
    TextField hourInput;

    @FXML
    Text activityName;

    @FXML
    private ListView<Activity> projectActivityListView;

    @FXML
    private BarChart<String,Number> registeredHours;

    @Override
    public void launch() {
        projectActivityListView.setItems(FXCollections.observableArrayList(getProjectManager().getEmployeeUsingSystem().getProjectActivities()));
        projectActivityListView.setCellFactory(projectActivityListView -> new ActivityCell());
        projectActivityListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->{
            selectedActivity = (ProjectActivity) newValue;
            prepareRegisterArea();
        });
        registerArea.setVisible(false);

        registeredHours.getXAxis().setLabel("Week");
        registeredHours.getYAxis().setLabel("Registered hours");
        registeredHours.setAnimated(false);
    }

    private void prepareRegisterArea(){
        if (selectedActivity != null){
            registerArea.setVisible(true);
            activityName.setText(selectedActivity.getName());

            weekInput.setValue(null);
            hourInput.setText("");
            weekInput.getStyleClass().remove("date-picker-error");
            hourInput.getStyleClass().remove("text-field-error");

            XYChart.Series<String,Number> hourSeries = new XYChart.Series<>();
            hourSeries.setName("Registered hours pr. week");
            ObservableList<XYChart.Data<String,Number>> hourData = hourSeries.getData();
            ArrayList<Timelog> loggedTimes = selectedActivity.getProgress(getProjectManager().getEmployeeUsingSystem());

            for (Timelog t: loggedTimes)
                hourData.add(new XYChart.Data<>(t.getDate().toString(), t.getHours()));

            registeredHours.getData().setAll(hourSeries);
        } else
            registerArea.setVisible(false);
    }

    @FXML
    public void registerTimeOnActivity(){
        boolean canRegisterHours = true;

        LocalDate localDate = weekInput.getValue();

        //Is the given date valid?
        if (localDate == null || !selectedActivity.isWithinTimeSpan(new SimpleDate(localDate))){
            canRegisterHours = false;
            weekInput.getStyleClass().add("date-picker-error");
        } else
            //User has supplied a valid date
            weekInput.getStyleClass().remove("date-picker-error");

        //Is the hour-count valid?
        try{
            String hourString = hourInput.getText();
            double hours = Double.valueOf(hourString);

            //Within bounds?
            if (hours > 0){
                //If no exception was thrown - the hours given were valid
                hourInput.getStyleClass().remove("text-field-error");
                if (canRegisterHours){
                    getProjectManager().addActivityHours(selectedActivity,new SimpleDate(localDate),hours);
                    prepareRegisterArea();
                }
            } else
                hourInput.getStyleClass().add("text-field-error");
        } catch (NumberFormatException e){
            hourInput.getStyleClass().add("text-field-error");
        } catch (InvalidOperationException e) {
            new ErrorDialog(e.getMessage()).showAndWait();
        }
    }
}