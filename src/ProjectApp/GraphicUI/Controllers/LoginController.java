package ProjectApp.GraphicUI.Controllers;

import ProjectApp.Employee;
import ProjectApp.GraphicUI.CustomCells.EmployeeCell;
import ProjectApp.GraphicUI.Dialogs.EmployeeDialog;
import ProjectApp.GraphicUI.Dialogs.ErrorDialog;
import ProjectApp.GraphicUI.SelectedScreen;
import ProjectApp.InvalidOperationException;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;

import java.util.Optional;

//Olav (s184195)
public class LoginController extends BaseController{
    private Employee selectedEmployee;

    @FXML
    Button logInButton;

    @FXML
    ListView<Employee> employeeListView;

    @Override
    public void launch() {
        employeeListView.getItems().setAll(getProjectManager().getEmployees());

        employeeListView.setCellFactory(param -> new EmployeeCell());
        employeeListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            selectedEmployee = newValue;
            logInButton.setDisable(false);
        });
        logInButton.setDisable(true);
    }

    @FXML
    public void logIn(){
        if (selectedEmployee != null){
            getProjectManager().setEmployeeUsingSystem(selectedEmployee);
            loadScreenInPlace(SelectedScreen.MAIN_DASHBOARD.toString());
        }
    }

    @FXML
    public void addEmployee(){
        EmployeeDialog dialog = new EmployeeDialog();
        boolean finished;
        do {
            Optional<String> result = dialog.showAndWait();
            if (result.isPresent()){
                try {
                    Employee createdEmployee = getProjectManager().addEmployee(result.get());
                    finished = true;

                    //Refresh listview
                    employeeListView.getItems().add(createdEmployee);
                    employeeListView.refresh();

                } catch (InvalidOperationException e) {
                    finished = false;
                    new ErrorDialog(e.getMessage()).showAndWait();
                }
            } else finished = true;
        } while (!finished);
    }
}