package ProjectApp.GraphicUI.Controllers;

import ProjectApp.GraphicUI.SelectedScreen;
import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

//Olav (s184195)
public class MainDashboardController extends BaseController {
    @FXML
    private AnchorPane rootPane;

    @FXML
    private Text initialText;

    @Override
    public void launch() {
        setupDashboard(rootPane);
        initialText.setText(getProjectManager().getEmployeeUsingSystem().getInitials());
        loadOverview();
    }

    @FXML
    public void loadOverview(){
        loadScreenAsChild(SelectedScreen.MAIN_OVERVIEW);
    }

    @FXML
    public void loadRegisterTime(){
        loadScreenAsChild(SelectedScreen.REGISTER_TIME);
    }

    @FXML
    public void loadAllProjects(){loadScreenAsChild(SelectedScreen.ALL_PROJECTS);}

    @FXML
    public void loadPersonalActivities(){loadScreenAsChild(SelectedScreen.PERSONAL_ACTIVITIES);}

    @FXML
    public void loadRequestHelp(){loadScreenAsChild(SelectedScreen.REQUEST_HELP);}

    @FXML
    public void logOut(){
        getProjectManager().setEmployeeUsingSystem(null);
        loadScreenInPlace(SelectedScreen.LOGIN.toString());
    }
}
