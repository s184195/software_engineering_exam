package ProjectApp.GraphicUI.Dialogs;

import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;

//Olav (s184195)
public class ActivityDialog extends Dialog {
    private boolean isProjectDialog;

    public ActivityDialog(boolean isProjectDialog){
        getDialogPane().getStylesheets().add(getClass().getResource("/application.css").toString());
        this.isProjectDialog = isProjectDialog;

        setHeaderText("Create an activity lasting in the given timespan");

        ButtonType createActivityButton = new ButtonType("Create", ButtonBar.ButtonData.OK_DONE);
        getDialogPane().getButtonTypes().addAll(createActivityButton, ButtonType.CANCEL);

        TextField nameTextInput = new TextField();
        DatePicker startDatePicker = new DatePicker();
        DatePicker endDatePicker = new DatePicker();
        TextField hoursNeededInput = new TextField();

        //Grid for elements
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        grid.add(new Label("Activity name:"),0,0);
        grid.add(nameTextInput,1,0);

        grid.add(new Label("Start week:"),0,1);
        grid.add(startDatePicker,1,1);

        grid.add(new Label("End week:"),0,2);
        grid.add(endDatePicker,1,2);
        String title = "";

        if (isProjectDialog){
            title = "Create new project activity";
            grid.add(new Label("Hours needed:"),0,3);
            grid.add(hoursNeededInput,1,3);
            setResultConverter(dialogButton -> {
                if(dialogButton == createActivityButton){
                    return new Pair<>(new Pair<>(nameTextInput.getText(),startDatePicker.getValue()),new Pair<>(endDatePicker.getValue(),hoursNeededInput.getText()));
                }
                return null;
            });
        } else{
            title = "Create new personal activity";
            setResultConverter(dialogButton -> {
                if(dialogButton == createActivityButton){
                    return new Pair<>(new Pair<>(nameTextInput.getText(),startDatePicker.getValue()),endDatePicker.getValue());
                }
                return null;
            });
        }
        setTitle(title);
        getDialogPane().setContent(grid);
    }
}
