package ProjectApp.GraphicUI.Dialogs;

import javafx.geometry.Insets;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

//Olav (s184195)
public class EmployeeDialog extends Dialog {
    public EmployeeDialog(){
        getDialogPane().getStylesheets().add(getClass().getResource("/application.css").toString());
        setTitle("Add a new employee to the system");
        setHeaderText("Initials must be unique and 4 letters long");

        ButtonType addEmployeeButton = new ButtonType("Add", ButtonBar.ButtonData.OK_DONE);
        getDialogPane().getButtonTypes().addAll(addEmployeeButton, ButtonType.CANCEL);

        TextField initialInput = new TextField();

        GridPane grid = new GridPane();
        grid.setVgap(10);
        grid.setHgap(10);

        grid.add(initialInput,0,0);

        grid.setPadding(new Insets(20, 150, 10, 10));
        setResultConverter(dialogButton -> {
            if (dialogButton == addEmployeeButton)
                return initialInput.getText();
            return null;
        });

        getDialogPane().setContent(grid);
    }
}
