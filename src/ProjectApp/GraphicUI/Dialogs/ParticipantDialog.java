package ProjectApp.GraphicUI.Dialogs;

import ProjectApp.Employee;
import ProjectApp.GraphicUI.CustomCells.EmployeeCell;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.layout.GridPane;

import java.util.ArrayList;

//Olav (s184195)
public class ParticipantDialog extends Dialog {
    public ParticipantDialog(ArrayList<Employee> employees){
        getDialogPane().getStylesheets().add(getClass().getResource("/application.css").toString());
        setTitle("Add an employee to the activity");
        setHeaderText("The employee must have less than 20 ongoing activities");

        ButtonType addEmployeeButton = new ButtonType("Add", ButtonBar.ButtonData.OK_DONE);
        getDialogPane().getButtonTypes().addAll(addEmployeeButton, ButtonType.CANCEL);

        ComboBox employeeSelector = new ComboBox();
        employeeSelector.setCellFactory(param -> new EmployeeCell());
        employeeSelector.setButtonCell(new EmployeeCell());
        employeeSelector.setItems(FXCollections.observableArrayList(employees));

        GridPane grid = new GridPane();
        grid.setVgap(10);
        grid.setHgap(10);

        grid.add(employeeSelector,0,0);

        grid.setPadding(new Insets(20, 150, 10, 10));
        setResultConverter(dialogButton -> {
            if (dialogButton == addEmployeeButton)
                return employeeSelector.getSelectionModel().getSelectedItem();
            return null;
        });

        getDialogPane().setContent(grid);
    }
}
