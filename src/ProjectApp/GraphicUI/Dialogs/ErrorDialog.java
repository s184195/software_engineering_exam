package ProjectApp.GraphicUI.Dialogs;

import javafx.scene.control.Alert;
import javafx.stage.StageStyle;

//Olav (s184195)
public class ErrorDialog extends Alert {
    public ErrorDialog(String errorMessage){
        super(AlertType.ERROR);
        setTitle("Invalid Operation");
        initStyle(StageStyle.UTILITY);
        setHeaderText(errorMessage);
    }
}
