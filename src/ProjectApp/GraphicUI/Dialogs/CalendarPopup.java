package ProjectApp.GraphicUI.Dialogs;

import ProjectApp.Activities.Activity;
import ProjectApp.GraphicUI.CustomCells.ActivityCell;
import javafx.collections.FXCollections;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.ListView;

import java.util.ArrayList;

//Olav (s184195)
public class CalendarPopup extends Dialog {
    public CalendarPopup(ArrayList<Activity> activities, int week){
        getDialogPane().getStylesheets().add(getClass().getResource("/application.css").toString());
        setTitle("Your activities in week " + week);

        ListView<Activity> activityListView = new ListView<>();
        activityListView.setCellFactory(param ->  new ActivityCell());
        activityListView.setItems(FXCollections.observableArrayList(activities));

        getDialogPane().getButtonTypes().addAll(ButtonType.OK);
        getDialogPane().setContent(activityListView);
    }
}
