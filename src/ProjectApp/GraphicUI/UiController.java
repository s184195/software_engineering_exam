package ProjectApp.GraphicUI;

import ProjectApp.GraphicUI.Controllers.BaseController;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

//Olav (s184195)
public class UiController extends BaseController{
    private Stage stage;
    private double xOffset;
    private double yOffset;

    private final double MIN_WIDTH = 900, MIN_HEIGHT = 600;

    @FXML
    HBox toolBar;

    @FXML
    HBox toolBarButtons;

    @FXML
    AnchorPane rootPane;

    @FXML
    Pane bottomDragPane;

    @FXML
    Pane rightDragPane;

    @FXML
    Pane cornerDragPane;

    @FXML
    public void exitProgram(){
        getProjectManager().exit();
        System.exit(0);
    }

    @FXML
    public void minimize(){
        stage.setIconified(true);
    }

    public void setStage(Stage stage){
        this.stage = stage;
    }

    public void addEventListeners(Stage primaryStage){
        //Moving window
        toolBar.setOnMousePressed(event -> {
            xOffset = primaryStage.getX() - event.getScreenX();
            yOffset = primaryStage.getY() - event.getScreenY();
        });

        toolBar.setOnMouseDragged(event -> {
            primaryStage.setX(event.getScreenX() + xOffset);
            primaryStage.setY(event.getScreenY() + yOffset);
        });

        //Resizing of window
        bottomDragPane.setOnMouseDragged(event -> {
            double height = event.getSceneY();
            if (height >= MIN_HEIGHT){
                primaryStage.setHeight(height);
                rescaleChildren();
            }
        });

        rightDragPane.setOnMouseDragged(event -> {
            double width = event.getSceneX();
            if (width >= MIN_WIDTH){
                primaryStage.setWidth(width);
                rescaleChildren();
            }
        });

        cornerDragPane.setOnMouseDragged(event -> {
            double height = event.getSceneY();
            if (height >= MIN_HEIGHT){
                primaryStage.setHeight(height);
                rescaleChildren();
            }

            double width = event.getSceneX();
            if (width >= MIN_WIDTH){
                primaryStage.setWidth(width);
                rescaleChildren();
            }
        });
    }

    @Override
    public void launch(){
        setRootPane(rootPane);
        loadScreenAsChild(SelectedScreen.LOGIN);
    }

    @FXML
    public void maximize(){
        stage.setMaximized(!stage.maximizedProperty().get());
        rescaleChildren();
    }

    public void styleToolbarButtons(int darknessLevel){
        String style = "";

        switch (darknessLevel){
            case 0: {
                style = "brightButton";
                break;
            }
            case 1: {
                style = "mediumButton";
                break;
            }
            case 2: {
                style = "darkButton";
                break;
            }
        }
        ObservableList<Node> buttons =  toolBarButtons.getChildren();
        for (Node b: buttons){
            b.getStyleClass().set(1,style);
        }
    }
}
