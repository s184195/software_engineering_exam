package ProjectApp.GraphicUI;

import ProjectApp.ProjectManager;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

//Olav (s184195)
public class ProjectMain extends Application {
    private UiController controller;
    private ProjectManager projectManager = new ProjectManager();;
    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage primaryStage) {
        try {

            primaryStage.setFullScreenExitHint("");
            primaryStage.initStyle(StageStyle.UNDECORATED);
            primaryStage.setTitle("Project System");

            /*String path = getClass().getResource("/application/resources/Graphics/icon.png").toExternalForm();
            Image icon = new Image(path);
            primaryStage.getIcons().add(icon);*/

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/uiControls.fxml"));

            //Consume warning from loading FXML
            Parent root = loader.load();

            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource("/application.css").toString());
            primaryStage.setScene(scene);

            // Gets reference to the controller
            controller = loader.getController();
            controller.setProjectManager(projectManager);
            controller.setStage(primaryStage);
            controller.addEventListeners(primaryStage);
            primaryStage.show();
            controller.launch();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void stop() throws Exception {
        projectManager.exit();
    }
}
