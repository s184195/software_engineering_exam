package ProjectApp.Activities;

import ProjectApp.Employee;
import ProjectApp.InvalidOperationException;
import ProjectApp.Project;
import ProjectApp.Timings.SimpleDate;

import java.util.ArrayList;

public class ProjectActivity extends Activity{
    private double hoursAllocated;
    private ArrayList<Employee> participants = new ArrayList<>();
    private Project parentProject;
    private ArrayList<Timelog> timelog = new ArrayList<>();

    public ProjectActivity(String name, double hours, SimpleDate startDate, SimpleDate endDate) throws InvalidOperationException {
        super(name, startDate,endDate);
        this.hoursAllocated = hours;
    }

    //Getters
    public ArrayList<Employee> getParticipants() {
        return participants;
    }

    public Project getParentProject() {
        return parentProject;
    }

    public ArrayList<Timelog> getTimelog() {
        return timelog;
    }

    public double getHoursAllocated() {
        return hoursAllocated;
    }

    //Setters
    public void setParentProject(Project parentProject) {
        this.parentProject = parentProject;
    }

    public void setHoursAllocated(double hoursAllocated) throws InvalidOperationException {
        if (hoursAllocated <= 0)
            throw new InvalidOperationException("Hours must be greater than 0");
        this.hoursAllocated = hoursAllocated;
    }

    //Wictor (s184197)
    public void registerHours(double hours, SimpleDate date, Employee e) throws InvalidOperationException {
        if (hours < 0)
            throw new InvalidOperationException("Hours registered is negative");
        if (!isWithinTimeSpan(date))
            throw new InvalidOperationException("Date is outside bounds of the activity");
        if (!participants.contains(e))
            throw new InvalidOperationException("You are not assigned to this activity.");
        timelog.add(new Timelog(e, date, hours));
    }

    //Olav (s184195)
    @Override
    public void updateStartDate(SimpleDate potentialDate) throws InvalidOperationException {
        //Ensures all the logged hours are within the timespan of the activity.
        for (Timelog t : timelog)
            if (potentialDate.compareTo(t.getDate()) < 0)
                throw new InvalidOperationException("An employee has registered hours before this date");

        //Test if moving the start date to an earlier date results in overbooking of employee
        int weeksMovedEarlier = potentialDate.compareTo(getStartDate());
        if (weeksMovedEarlier > 0){
            for (Employee e: participants){
                int[] freeWeeks = e.occupiedWeeks(potentialDate, getEndDate());

                //Simulate the activity spans from the new date
                for (int i = 0; i < weeksMovedEarlier; i++ ){
                    if(Math.abs(freeWeeks[i]) > Employee.MAX_PROJECTS)
                        throw new InvalidOperationException("Moving date results in overbooking of employee " + e.getInitials());
                }
            }
        }
        super.updateStartDate(potentialDate);
    }

    //Olav (s184195)
    @Override
    public void updateEndDate(SimpleDate potentialDate) throws InvalidOperationException {
        //Ensures all the logged hours are within the timespan of the activity.
        for (Timelog t : timelog)
            if (t.getDate().compareTo(potentialDate) < 0)
                throw new InvalidOperationException("An employee has registered hours after this date");

        //Test if moving the end date to a later date results in overbooking of employee
        int weeksMovedLater = getEndDate().compareTo(potentialDate);
        if (weeksMovedLater > 0) {
            for (Employee e : participants) {
                int[] freeWeeks = e.occupiedWeeks(getStartDate(), potentialDate);
                int startIndex = getStartDate().compareTo(potentialDate);

                //Simulate the activity spans from the new date
                for (int i = 0; i < weeksMovedLater; i++) {
                    if (Math.abs(freeWeeks[i + startIndex]) > Employee.MAX_PROJECTS)
                        throw new InvalidOperationException("Moving date results in overbooking of employee " + e.getInitials());
                }
            }
        }
        super.updateEndDate(potentialDate);
    }

    //Wictor (s184197)
    //Compute total used hours
    public double getHours() {
        double hours = 0;
        for (Timelog t : timelog)
              hours += t.getHours();
        return hours;
    }

    //Wictor (s184197)
    //Compute hours used by specific employee
    public double getHours(Employee e) {
        double hours = 0;
        for (Timelog t : timelog)
            if (t.getEmployee() == e)
                hours += t.getHours();
        return hours;
    }

    //Wictor (s184197)
    //Return all logs registered by the given employee
    public ArrayList<Timelog> getProgress(Employee e) {
        ArrayList<Timelog> log = new ArrayList<>();
        for (Timelog t : timelog) {
            if (t.getEmployee().equals(e))
                log.add(t);
        }
        return log;
    }

    //Olav (s184195)
    public double[] getWeeklyProgress(){
        int spanningWeeks = getStartDate().compareTo(getEndDate()) + 1;

        double[] weeklyProgress = new double[spanningWeeks];

        for (Timelog t: timelog){
            int index = getStartDate().compareTo(t.getDate());
            weeklyProgress[index] += t.getHours();
        }
        return weeklyProgress;
    }

    public  void addParticipant(Employee e) throws InvalidOperationException {
        if (e.isFreeWithinPeriod(getStartDate(),getEndDate())){
            participants.add(e);
            e.addProjectActivity(this);
        }
        else throw new InvalidOperationException("Employee is not free in given period");
    }
}