package ProjectApp.Activities;

import ProjectApp.InvalidOperationException;
import ProjectApp.Timings.SimpleDate;

//Björn s184214
public class PersonalActivity extends Activity {
	public PersonalActivity(String name, SimpleDate startDate, SimpleDate endDate) throws InvalidOperationException {
		super(name, startDate, endDate);
	}
}
