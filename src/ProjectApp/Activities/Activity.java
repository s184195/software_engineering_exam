package ProjectApp.Activities;

import ProjectApp.InvalidOperationException;
import ProjectApp.Timings.SimpleDate;

//Olav (s184195)
public abstract class Activity {
    protected String name;
    private SimpleDate startDate;
    private SimpleDate endDate;

    public Activity(String name, SimpleDate startDate, SimpleDate endDate) throws InvalidOperationException {
        this.startDate = startDate;
        this.endDate = endDate;
        this.name = name;
        if (startDate.compareTo(endDate) < 0)
            throw new InvalidOperationException("End date is smaller than starting date");
    }

    public SimpleDate getStartDate() {
        return startDate;
    }

    public SimpleDate getEndDate() {
        return endDate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void updateStartDate(SimpleDate potentialDate) throws InvalidOperationException {
        //Ensure the new start date is still earlier then the end date
        if (potentialDate.compareTo(endDate) < 0)
            throw new InvalidOperationException("The new date would be later than the end date");

        startDate.setWeek(potentialDate.getWeek());
        startDate.setYear(potentialDate.getYear());
    }

    public void updateEndDate(SimpleDate potentialDate) throws InvalidOperationException {
        //Ensure the new start date is still earlier then the end date
        if (potentialDate.compareTo(startDate) > 0)
            throw new InvalidOperationException("The new date would be earlier than the start date");

        endDate.setWeek(potentialDate.getWeek());
        endDate.setYear(potentialDate.getYear());
    }

    public boolean isWithinTimeSpan(SimpleDate otherDate){
        return  startDate.compareTo(otherDate) >= 0 && endDate.compareTo(otherDate) <= 0;
    }
}