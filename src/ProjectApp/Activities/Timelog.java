package ProjectApp.Activities;

import ProjectApp.Employee;
import ProjectApp.Timings.SimpleDate;

//Wictor (s184197)
public class Timelog {
    private Employee employee;
    private SimpleDate date;
    private double hours;

    public Employee getEmployee() {
        return employee;
    }

    public SimpleDate getDate() {
        return date;
    }

    public double getHours() {
        return hours;
    }

    public Timelog(Employee e, SimpleDate d, double h){
        this.employee = e;
        this.date = d;
        this.hours = h;
    }
}
