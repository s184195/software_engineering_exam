package ProjectApp;

import ProjectApp.Activities.Activity;
import ProjectApp.Activities.PersonalActivity;
import ProjectApp.Activities.ProjectActivity;
import ProjectApp.Database.Database;
import ProjectApp.Database.DatabaseJSON;
import ProjectApp.Timings.SimpleDate;
import ProjectApp.Timings.TimeManager;

import java.util.ArrayList;

public class ProjectManager {
    private ArrayList<Project> projects = new ArrayList<>();
    private ArrayList<Employee> employees = new ArrayList<>();
    private TimeManager timeManager;
    private Database database = new DatabaseJSON();

    private Employee employeeUsingSystem; //The current user of the system
    private Project selectedProject;      //The project shown to the user

    public ProjectManager(){
        timeManager = new TimeManager();
        database.load(this);
    }

    /*
    * GETTERS & SETTERS
    */

    public void setEmployeeUsingSystem(Employee employeeUsingSystem) {
        this.employeeUsingSystem = employeeUsingSystem;
    }

    public Employee getEmployeeUsingSystem() {
        return employeeUsingSystem;
    }

    public ArrayList<Project> getProjects() {
        return projects;
    }

    public TimeManager getTimeManager() {
        return timeManager;
    }

    public ArrayList<Employee> getEmployees() {
        return employees;
    }

    public Project getSelectedProject() {
        return selectedProject;
    }

    public void setSelectedProject(Project selectedProject) {
        this.selectedProject = selectedProject;
    }

    //Returns the employee with the given initials - if it exists
    //Wictor (s184197)
    public Employee getEmployee(String inititals){
        for (Employee employee: employees){
            if (inititals.equals(employee.getInitials())) {
                return employee;
            }
        }
        return null;
    }

    /*
     * Methods
     */

    //Wictor (s184197)
    public void addActivityHours(ProjectActivity activity, SimpleDate date, double hours) throws InvalidOperationException {
        activity.registerHours(hours, date, employeeUsingSystem);
    }

    //Olav (s184195)
    //Creates a new employee with the given initials
    public Employee addEmployee(String initials) throws InvalidOperationException {
        if (initials.trim().length() != 4)
            throw new InvalidOperationException("Initials must be exactly 4 characters long");

        for (int i = 0; i < 4; i++)
            if (!Character.isAlphabetic(initials.charAt(i)))
                throw new InvalidOperationException("Initials must be alphabetic");

        initials = initials.toUpperCase();

        //All initials must be unique
        for (Employee currentEmployee: employees){
            if (initials.equals(currentEmployee.getInitials()))
                throw new InvalidOperationException("Initials are not unique");
        }

        Employee newEmployee = new Employee(initials);
        employees.add(newEmployee);
        return newEmployee;
    }

    //Olav (s184195)
    public Project createProject() throws InvalidOperationException {return createProject(null);}

    //Olav (s184195)
    //Creates and adds a new project to the system, if the name is unique
    public Project createProject(String projectName) throws InvalidOperationException{
        Project newProject;

        if (projectName == null)                                        //1
            newProject = new Project(timeManager.generateId());

        else {
            //Tests whether name is unique
            for (Project p: projects)
                if (projectName.equals(p.getProjectName()))             //2
                    throw new InvalidOperationException("Project name already in use.");

            newProject = new Project(timeManager.generateId(),projectName);
        }
        projects.add(newProject);
        return newProject;
    }

    public Project getProject(int projectID){
        for (Project project: projects)
            if (projectID == project.getId())
                return project;
        return null;
    }

    //Wictor (s184197)
    public Project getProject(String name){
        for (Project p : projects)
            if (name.equals(p.getProjectName()))
                return p;
        return null;
    }

    public void appointProjectLeader(Project p, Employee e) throws InvalidOperationException {
        p.setProjectLeader(e);
    }

    public void addEmployeeToProjectActivity(Project p, ProjectActivity a, Employee e) throws InvalidOperationException {
        p.addEmployeeToProjectActivity(a,e);
    }

    public ProjectActivity createActivityInProject(Project p, String name, double hours, SimpleDate startWeek, SimpleDate endWeek) throws InvalidOperationException {
        if(!employeeUsingSystem.equals(p.getProjectLeader()))
            throw new InvalidOperationException("You are not the project leader!");
        return p.createProjectActivity(name, hours, startWeek, endWeek);
    }

    public PersonalActivity makePersonalActivityForEmployee(Employee e, String name, SimpleDate startDate, SimpleDate endDate) throws InvalidOperationException {
        return e.makePersonalActivity(name,startDate,endDate);
    }

    //Olav (s184195)
    public ArrayList<Employee> getAvailableEmployees(SimpleDate startDate, SimpleDate endDate){
        ArrayList<Employee> freeEmployees = new ArrayList<>();
        for (Employee e: employees){
            if (e.isFreeWithinPeriod(startDate,endDate))
                freeEmployees.add(e);
        }
        return freeEmployees;
    }

    //Wictor (s184197)
    public void requestHelp(Employee askedEmployee, ProjectActivity a) throws InvalidOperationException{
        a.addParticipant(askedEmployee);
    }

    public void updateStartDateInActivity(Activity a, SimpleDate newDate) throws InvalidOperationException {
        a.updateStartDate(newDate);
    }

    public void updateEndDateInActivity(Activity a, SimpleDate newDate) throws InvalidOperationException {
        a.updateEndDate(newDate);
    }

    public void updateProjectActivityName(Project p, ProjectActivity a, String newName) throws InvalidOperationException {
        p.updateProjectActivityName(a,newName);
    }

    public void exit(){
        database.save();
    }
}