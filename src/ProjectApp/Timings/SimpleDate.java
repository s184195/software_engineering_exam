package ProjectApp.Timings;

import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.Locale;

//Björn s184214
public class SimpleDate implements Comparable<SimpleDate>{
	private int year;
	private int weekNum;
	//Björn s184214
	public SimpleDate(int year, int weekNum) {
		this.year = year;
		this.weekNum = weekNum;
	}

	//Olav (s184195)
	public SimpleDate(LocalDate localDate){
		WeekFields weekFields = WeekFields.of(Locale.getDefault());
		LocalDate lastDayOfWeek = localDate.with(weekFields.dayOfWeek(),7);

		this.weekNum = lastDayOfWeek.get(WeekFields.ISO.weekOfWeekBasedYear());
		this.year = lastDayOfWeek.getYear();
	}
	public void setYear(int year) {
		this.year = year;
	}
	public void setWeek(int week) {
		this.weekNum = week;
	}

	public int getYear() {
		return this.year;
	}
	public int getWeek() {
		return this.weekNum;
	}


	@Override
	//Returns difference in weeks between 2 dates
	//Björn s184214
	public int compareTo(SimpleDate o) {
		int totalDiff = (o.getYear()-this.year)*52+(o.getWeek()-this.weekNum);
		return totalDiff;
	}

	@Override
	public String toString() {
		return String.format("Week %02d, %d",weekNum, year%100);
	}

	//Olav (s184195)
	//Returns SimpleDate as LocalDate - defaulting to the first day in the week
	public LocalDate toLocalDate(){
		WeekFields weekFields = WeekFields.of(Locale.getDefault());
		LocalDate asLocalDate = LocalDate.now()
										.withYear(year)
										.with(weekFields.weekOfYear(),weekNum)
										.with(weekFields.dayOfWeek(),7);
		return asLocalDate;
	}
}