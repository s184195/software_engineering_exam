package ProjectApp.Timings;

import java.time.LocalDate;

//Olav (s184195)
public class TimeManager {
    private int workingYear;
    private int projectsCreatedThisYear = 0;

    public int getWorkingYear() {
        return workingYear;
    }

    public void setWorkingYear(int workingYear) {
        this.workingYear = workingYear;
    }

    public int getProjectsCreatedThisYear() {
        return projectsCreatedThisYear;
    }

    public void setProjectsCreatedThisYear(int projectsCreatedThisYear) {
        this.projectsCreatedThisYear = projectsCreatedThisYear;
    }

    public TimeManager(){
        workingYear = LocalDate.now().getYear();
    }

    public int updateYear(){
        int currentYear = LocalDate.now().getYear();
        if (currentYear != workingYear){
            workingYear = currentYear;
            projectsCreatedThisYear = 0;
        }
        return currentYear;
    }

    public int generateId(){
    	updateYear();
        projectsCreatedThisYear++;
        return Integer.valueOf("" + projectsCreatedThisYear + workingYear%100);
    }
}
