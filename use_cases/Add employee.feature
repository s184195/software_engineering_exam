Feature: Add employee to project
  Description: The project leader adds an employee to the project
  Actor: Project leader

  Background:
    Given that a project "Bar" exists
    Given that the employee "JANE" exists
    Given that the employee "JOHN" exists

# Main scenario
  #Wictor (s184197)
  Scenario: Project leader adds employee to project
    Given that "JOHN" is the project leader of "Bar"
    When "JOHN" adds "JANE" to project "Bar"
    Then "JANE" is participating on "Bar"
    And "Bar" has "JANE" as participant
    
## Björn s184214    
##Extended Scenario
  Scenario: Project Leaders add one employee to multiple projects
  Given that "JOHN" is the project leader of "Bar"
  Given that the employee "JACK" exists
  Given that a project "Foo" exists
  Given that "JACK" is the project leader of "Foo"
  When "JOHN" adds "JANE" to project "Bar"
  Then "JANE" is participating on "Bar"
  And "Bar" has "JANE" as participant
  When "JACK" adds "JANE" to project "Foo"
  Then "JANE" is participating on "Foo"
  And "Foo" has "JANE" as participant
  

# Error Scenario
  #Wictor (s184197)
  Scenario: Project leader adds an employee who is already on the project
    Given that "JOHN" is the project leader of "Bar"
    And that "JANE" is a participant of "Bar"
    When "JOHN" adds "JANE" to project "Bar"
    Then the employee gets the error message "Employee is already on the project."

# Error Scenario
  #Wictor (s184197)
  Scenario: Employee on project adds other employee
    Given that "JANE" is a participant of "Bar"
    Given that the employee "JACK" exists
    And that "JOHN" is the project leader of "Bar"
    When "JANE" adds "JACK" to project "Bar"
    Then the employee gets the error message "You do not have permission to do this."