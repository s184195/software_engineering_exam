Feature: Register Time
  Actor: Employee

  Background: Initialize
    Given that a project "Bar" exists
    Given that activity "Time" exists in "Bar" from week 1, 2019 to week 5, 2019
    Given that the employee "JANE" exists
    Given that the employee "JOHN" exists

  #Wictor (s184197)
  Scenario: Employee who is not a participant tries to log hours.
    Given that "JANE" is not a participant of "Time" in "Bar"
    And that "Time" in "Bar" has 0.0 hours logged
    When "JANE" registers 4.5 hours spent on "Time" in "Bar" on week 1, 2019
    And "Time" in "Bar" has 0.0 hours logged in week 1, 2019
    And the employee gets the error message "You are not assigned to this activity."

  #Wictor (s184197)
  Scenario: Participant logs hours to date.
    Given that "JANE" is a participant of "Time" in "Bar"
    And that "Time" in "Bar" has 0.0 hours logged
    When "JANE" registers 4.5 hours spent on "Time" in "Bar" on week 1, 2019
    Then "Time" in "Bar" has 4.5 hours logged for "JANE" in week 1, 2019
    And "Time" in "Bar" has 4.5 hours logged in week 1, 2019
    And "JANE" has 4.5 hours logged on "Time" in "Bar"

  Scenario: Participant logs hours outside of activity dates
    Given that "JANE" is a participant of "Time" in "Bar"
    And that "Time" in "Bar" has 0.0 hours logged
    When "JANE" registers -1.5 hours spent on "Time" in "Bar" on week 1, 2019
    Then the employee gets the error message "Hours registered is negative"


  Scenario: Participant logs hours outside of activity dates
    Given that "JANE" is a participant of "Time" in "Bar"
    And that "Time" in "Bar" has 0.0 hours logged
    When "JANE" registers 4.5 hours spent on "Time" in "Bar" on week 7, 2019
    Then the employee gets the error message "Date is outside bounds of the activity"