## Björn s184214
Feature: Calculate Project Progress
Description: Calculate the total amount of time spent on the given project
Actor: Employee

Background:
	Given that a project "Foo" exists
	Given the project "Foo" has a project activity "Do stuff"
	And "Do stuff" in "Foo" has 0.0 hours logged in week 2, 2000
	Given the project "Foo" has a project activity "Do more stuff"
	And "Do more stuff" in "Foo" has 0.0 hours logged in week 3, 2000
	Given that the employee "JANE" exists
	Given that "JANE" is a participant of "Do stuff" in "Foo"
	Given that "JANE" is a participant of "Do more stuff" in "Foo"
	
	
Scenario: The system computes the total time spent on a given project
	When the system calculates the project progress for the project "Foo" the value 0 is returned
	When "JANE" registers 1.0 hours spent on "Do stuff" in "Foo" on week 2, 2000
	When the system calculates the project progress for the project "Foo" the value 1 is returned
	When "JANE" registers 2.0 hours spent on "Do stuff" in "Foo" on week 3, 2000
	When the system calculates the project progress for the project "Foo" the value 3 is returned
	
	
	 