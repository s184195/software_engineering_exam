Feature: Convert between SimpleDate and LocalDate
Description: Test of change in data when changing between SimpleDate and LocalDate

Scenario: Convert from SimpleDate to LocalDate and back
  Given a Simple Date X with year 2019 and weekNum 14
  When converting X to a LocalDate
  Then X has the LocalDate fields 2019 and 14
  When converting X to a SimpleDate
  Then X has the SimpleDate fields 2019 and 14

Scenario: Convert from LocalDate to SimpleDate and back
  Given a LocalDate X with year 2019 and week 14
  When converting X to a SimpleDate
  Then X has the SimpleDate fields 2019 and 14
  When converting X to a LocalDate
  Then X has the LocalDate fields 2019 and 14