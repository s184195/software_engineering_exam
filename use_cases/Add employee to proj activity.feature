Feature: Add employee to project activity
  Description: The project leader adds an employee to a project activity
  Actor: Project leader
  
  Background:
  	Given a project "Bar" with a project leader "IDEA"
	Given that the employee "JACK" exists
	Given the project "Bar" has a project activity "Do stuff"
	
# Main scenario
  Scenario: Project leader adds first employee to project activity
    When the project leader of "Bar" adds "JACK" to "Do stuff"
    Then "JACK" is part of the list of employees working on the activity "Do stuff"
    And the list of activities for the employee "JACK" contains the activity "Do stuff"

#Extended Scenario
## Björn s184214
 Scenario: Project leader adds an employee to project activity that already has employees
 	Given that the employee "JACK" is working on the activity "Do stuff"
 	Given that the employee "JANE" exists
 	Given that the employee "JANE" is working on the activity "Do stuff"
 	Given that the employee "JOHN" exists
    When the project leader of "Bar" adds "JOHN" to "Do stuff"    
    Then "JOHN" is part of the list of employees working on the activity "Do stuff"
    And the list of activities for the employee "JOHN" contains the activity "Do stuff"

## Björn s184214
 Scenario: Project leader adds an employee to multiple activities
 	Given the project "Bar" has a project activity "Do more stuff"
 	When the project leader of "Bar" adds "JACK" to "Do stuff"
    Then "JACK" is part of the list of employees working on the activity "Do stuff"
    And the list of activities for the employee "JACK" contains the activity "Do stuff"
    When the project leader of "Bar" adds "JACK" to "Do more stuff"
    Then "JACK" is part of the list of employees working on the activity "Do more stuff"
    And the list of activities for the employee "JACK" contains the activity "Do more stuff"

## Björn s184214
Scenario: Project leader attempts to add an activity to an occupied employeer
	Given that the employee "JACK" is participating on "Bar"
	Given that the employee "JACK" has 20 project activities at the same time
	Given that the employee "JACK" has a personal activtity in the given period
	When the project leader of "Bar" adds "JACK" to "Do stuff"
	Then the project leader gets the error message "Employee is not free in given period" 	