Feature: Request Help
  Actor: Employee

  Background: Initialize
    Given that a project "Bar" exists
    Given that activity "Time" exists in "Bar"
    Given that the employee "JANE" exists
    Given that the employee "JOHN" exists

#Main Scenario
  #Wictor (s184197)
  Scenario: Participant asks for help.
    Given that "JOHN" is a participant of "Time" in "Bar"
    And that "Time" in "Bar" has 0.0 hours logged
    When "JOHN" asks "JANE" for help with "Time" in project "Bar"
    Then "JANE" is a participant of "Time" in "Bar"
    And "JANE" is not a participant of "Bar"
    #Er resten nødvendige?
    When "JANE" registers 4.0 hours spent on "Time" in "Bar" on week 1, 2019
    Then "Time" in "Bar" has 4.0 hours logged