Feature: Create project
  Description: An employee creates a new project
  Actor: Employee

# Main scenario
  Scenario: Employee successfully creates project
    Given that the employee "JANE" exists
    When the employee creates a new nameless project
    Then the new project is added to the list of projects

# Alternative scenario
  Scenario: Employee creates project with a name
    Given that the employee "JANE" exists
    When the employee creates a project "Foo"
    Then a new project is created with project name "Foo"

# Extended Scenario
## Björn s184214
 Scenario: Employee creates multiple projects with and without name
 	Given that the employee "JANE" exists
 	When the employee creates a new nameless project
 	Then the new project is added to the list of projects
 	When the employee creates a new nameless project
 	Then the new project is added to the list of projects
 	When the employee creates a project "Foo"
    Then a new project is created with project name "Foo"

# Error scenario
  Scenario: Employee creates project with a name which is already used
    Given that the employee "JANE" exists
    And that a project "Foo" exists
    When the employee creates a project "Foo"
    Then the employee gets the error message "Project name already in use."