Feature: Create project activity
  Description: The project leader creates a new project activity
  Actor: Project leader

# Main scenario
  Scenario: The project leader creates a new project activity
    Given a project "Bar" with a project leader "IDEA"
    When the employee "IDEA" creates a project activity "Work" with estimated time 4 hours and starting on week 1 in 2019 ending on week 3 in 2019
    Then the project has a project activity called "Work" with 4 allocated hours spanning from week 1 to 3
    
 Scenario: The project leader attempts to create a project activity that ends before it begins
 	Given a project "Bar" with a project leader "IDEA"
 	When the employee "IDEA" creates a project activity "Work" with estimated time 40 hours and starting on week 3 in 2020 ending on week 43 in 2003
 	Then the employee gets the error message "End date is smaller than starting date"

   
 Scenario: An employee attempts to create a project activity, while not being project leader
   Given a project "Bar" with a project leader "IDEA"
   And that the employee "JANE" exists
   And that "JANE" is not the project leader of "Bar"
   When the employee "JANE" creates a project activity "Bar" with estimated time 10 hours and starting on week 17 in 2019 ending on week 20 in 2019
   Then the employee gets the error message "You are not the project leader!"