## Björn s184214
Feature: Create personal activity
Description: An employee creates a new personal activity
Actor: Employee
# Main scenario

Scenario: An employee succesfully creates a new personal activity
Given that the employee "JOHN" exists
When the employee creates a personal activity with the reason "Vacation" from week 1 and year 2019 to week 3 and year 2019
Then the personal activity appears in the employees list of activities

#Extended scenario
Scenario: An employee create multiple new personal actvities
Given that the employee "JOHN" exists
When the employee creates a personal activity with the reason "Vacation" from week 10 and year 2019 to week 14 and year 2019
Then the personal activity appears in the employees list of activities
When the employee creates a personal activity with the reason "Illness" from week 1 and year 2019 to week 2 and year 2019
Then the personal activity appears in the employees list of activities


#Fail Scenario
Scenario: An employee creates a new personal activity with reversed dates
Given that the employee "JOHN" exists
When the employee creates a personal activity with the reason "Vacation" from week 1 and year 2020 to week 48 and year 2019
Then the employee gets the error message "End date is smaller than starting date"

##All complete