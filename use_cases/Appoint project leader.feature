Feature: Appoint project leader
  Description: An employee is appointed project leader
  Actor: Employee

  Background:
    Given that a project "Bar" exists
    Given that the employee "JANE" exists
    Given that the employee "JOHN" exists

# Main scenario
  Scenario: Employee appoints a project leader
    When an employee appoints "JOHN" as project leader
    Then the project’s leader is "JOHN"

# Error scenario
  Scenario: Employee appoints a project leader but the project already has a leader
    And that "JANE" is the project leader of "Bar"
    When an employee appoints "JOHN" as project leader
    Then the employee gets the error message "Project already has a project leader"