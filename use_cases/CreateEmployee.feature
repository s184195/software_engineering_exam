## Björn s184214
Feature: Create a new employee
 Description: A User creates an new employee
 Actor: Program user
 
 ## Main scenario
 Scenario: A new employee account is successfully created
 	When the user creates an employee with name "JANE"
 	Then an employee with name "JANE" is successfully created
 	
##Extended scenario
Scenario: A new employee account is successfully created and added to to group of preexisting users
	Given that the employee "JOHN" exists
	Given that the employee "JACK" exists
 	When the user creates an employee with name "JANE"
 	Then an employee with name "JANE" is successfully created
	 
 ## Fail Scenario
Scenario: The user attempts to create a new employee account with less than 4 letters
 	When the user creates an employee with name "JOE"
 	Then the user gets the error message "Initials must be exactly 4 characters long"
 	
 ## Fail Scenario
 Scenario: The user attempts to create a new employee account with more than 4 letters
 	When the user creates an employee with name "JARED"
 	Then the user gets the error message "Initials must be exactly 4 characters long"
 	
## Fail scenario
Scenario: The user attempt to create a new employee account with a non alphabetical name
	When the user creates an employee with name "G0tU"
 	Then the user gets the error message "Initials must be alphabetic"

## Fail scenario
Scenario: The user attempts to create a new employee account without a unique account name
	Given that the employee "JANE" exists
	When the user creates an employee with name "JANE"
 	Then the user gets the error message "Initials are not unique"