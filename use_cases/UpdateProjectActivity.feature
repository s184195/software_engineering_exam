Feature: Update fields of project activities
Description: Update different fields of a project activity
Actor: User

Background: Initialize
  Given that a project "Bar" exists
  Given that activity "Time" exists in "Bar" from week 1, 2019 to week 5, 2019

Scenario: Change name of activity
  Given the project "Bar" does not have an activity "Rod"
  When the project leader changes the name of "Time" in "Bar" to "Rod"
  Then the project "Bar" has a project activity "Rod"
  And there is no activity called "Time" in "Bar"

#Error scenario
Scenario: Change name of activity to an existing one
  Given that activity "Rod" exists in "Bar" from week 5, 2019 to week 20, 2019
  When the project leader changes the name of "Rod" in "Bar" to "Time"
  Then the user gets the error message "Project activity name is not unique"
  And the project "Bar" has a project activity "Time"
  And the project "Bar" has a project activity "Rod"

#Error scenario
Scenario: Change name of activity to empty
    When the project leader changes the name of "Time" in "Bar" to ""
    Then the user gets the error message "Project activity name may not be empty"
    And the project "Bar" has a project activity "Time"

Scenario: Change starting date of activity
  When the project leader changes the starting date of "Time" in "Bar" to week 3, 2019
  Then the project "Bar" has a project activity called "Time" spanning from week 3, 2019 to 5, 2019

Scenario: Change ending date of activity
    When the project leader changes the end date of "Time" in "Bar" to week 3, 2019
    Then the project "Bar" has a project activity called "Time" spanning from week 1, 2019 to 3, 2019

#Error scenario
Scenario: Change start date to overlap
  When the project leader changes the starting date of "Time" in "Bar" to week 6, 2019
  Then the project leader gets the error message "The new date would be later than the end date"
  And the project "Bar" has a project activity called "Time" spanning from week 1, 2019 to 5, 2019

#Error scenario
Scenario: Change end date to overlap
  When the project leader changes the end date of "Time" in "Bar" to week 49, 2018
  Then the project leader gets the error message "The new date would be earlier than the start date"
  And the project "Bar" has a project activity called "Time" spanning from week 1, 2019 to 5, 2019