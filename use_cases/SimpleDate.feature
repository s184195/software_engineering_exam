## Björn s184214
Feature: Simple Date
Description: Compare dates correctly
Actor: None

Scenario: A date X is before date Y in the same year
Given a Simple Date X with year 2019 and weekNum 12
And a Simple Date Y with year 2019 and weekNum 18
Then the compareTo function returns 6

Scenario: A date X is before date Y in a different year
Given a Simple Date X with year 2019 and weekNum 12
And a Simple Date Y with year 2020 and weekNum 18
Then the compareTo function returns 58

Scenario: A date X is after date Y in a the same year
Given a Simple Date X with year 2019 and weekNum 43
And a Simple Date Y with year 2019 and weekNum 22
Then the compareTo function returns -21

Scenario: A date X is after date Y in a different year
Given a Simple Date X with year 2020 and weekNum 43
And a Simple Date Y with year 2019 and weekNum 22
Then the compareTo function returns -73

